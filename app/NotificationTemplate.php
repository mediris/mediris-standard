<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class NotificationTemplate extends RemoteModel {
    public static $formatted_fields = [
                [   "field" => "{{procedure}}",
                    "es" => "Nombre del estudio",
                    "en" => "Procedure name" ],
                [   "field" => "{{date}}",
                    "es" => "Fecha de finalización de la orden",
                    "en" => "Order end date" ],
                [   "field" => "{{patient_name}}",
                    "es" => "Nombre del paciente",
                    "en" => "Patient name" ],
                [   "field"=>"{{link_poll}}",
                    "es" => "link de encuestas",
                    "en" => "survey link" 
                    ]
    ]; 

    public function __construct() {
        $this->apibase = 'api/v1/notification_templates';
        parent::__construct();
    }

    /**
     * @fecha: 15-06-2017
     * @parametros:
     * @programador: Hendember Heras
     * @objetivo: Busca las instancias de NotificationTemplate que esten activos.
     * @change: sustituye al método getActive
     */
    public static function remoteFindActive() {
        return parent::remoteFindByAction('/showactive'); 
    }
}
