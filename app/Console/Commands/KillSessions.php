<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\FileSessionHandler;
use Illuminate\Filesystem\Filesystem;


class KillSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kill:sessions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cerrar todas las sessiones activas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // $request->session()->getHandler()->destroy($request->session()->getId());
        // $request->session()->flush();

            $fileSession = new FileSessionHandler( new Filesystem, storage_path('framework/sessions'), 120);
            // $fileSession->destroy($request->session()->getId());
            $fileSession->gc(0);

         // Auth::logout();
         // $this->info('Todas las sesiones han sido cerradas.');
    }
}
