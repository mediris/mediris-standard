<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;
use DB;

class Section extends Model {
    use Rememberable;

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name', 'parent_id',
    ];

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Section tiene muchos Actions.
     */
    public function actions() {
        return $this->belongsToMany(Action::class)->withPivot('id');
    }
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Section tiene un Section Padre.
     */
    public function parentSection() {
        return $this->hasOne(Section::class, 'id', 'parent_id');
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Section tiene muchos Sections hijos.
     */
    public function childSections() {
        return $this->hasMany(Section::class, 'parent_id', 'id');
    }

    /**
     * @fecha: 10-04-2017
     * @programador: Diego Oliveros
     * @objetivo: Relación: Un cargo tiene muchas Secciones.
     */
    public function charges() {
        return $this->belongsToMany(Charge::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }

    /**
     * @fecha: 17-11-2017
     * @programador: Alton Bell Smythe
     * @objetivo: Obtener todas las sections inactivas.
     */
    public static function getUnactive() {
        return DB::table('sections AS s')
            ->select('s.id AS `id`',
                     's.name AS `section`')
            ->where('s.active', '=' , 0)
            ->get();

    }

}