<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Consumable extends Model {
    public function __construct() {
        $this->client = new Client();
        $this->url = 'api/v1/consumables';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos
     * @programador: Juan Bigorra /Pascual Madrid * @objetivo:Función para obtener una colección de Consumables desde
     *     el api .
     */
    public function getAll( $url ) {
        $response = $this->client->request('POST', $url . $this->url, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token ] ]);
        $consumables = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $consumable = new Consumable();

            foreach ( $element as $key => $value ) {
                $consumable->$key = $value;

            }

            $consumables->push($consumable);
        }

        return $consumables;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador del
     *     elemento a buscar
     * @programador: Juan Bigorra /Pascual Madrid * @objetivo:Función para obtener una instancia de Consumable dado un
     *     identificador .
     */
    public function get( $url, $id ) {
        $response = $this->client->request('POST', $url . $this->url . '/show/' . $id, [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);
        $consumable = new Consumable();
        foreach ( json_decode($response->getBody()) as $key => $value ) {
            $consumable->$key = $value;
        }

        return $consumable;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $data = Los datos a ser
     *     guardados
     * @programador: Juan Bigorra /Pascual Madrid * @objetivo:Función para crear una nueva instancia de Consumable .
     */
    public function add( $url, $data ) {
        $res = $this->client->request('POST', $url . $this->url . '/add', [ 'headers' => $this->headers, 'form_params' => $data ]);

        return $res;

    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $data = Los datos a ser
     *     editados, $id = Identificador de la instancia a ser editada
     * @programador: Juan Bigorra /Pascual Madrid * @objetivo:Función para editar una instancia de Consumable dado un
     *     identificador .
     */
    public function edit( $url, $data, $id ) {
        $res = $this->client->request('POST', $url . $this->url . '/edit/' . $id, [ 'headers' => $this->headers, 'form_params' => $data ]);

        return $res;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador de la
     *     instancia
     * @programador: Juan Bigorra /Pascual Madrid * @objetivo:Función para eliminar una instancia de Consumable dado un
     *     identificador . NO ESTÁ EN USO
     */
    public function remove( $url, $id ) {
        $res = $this->client->request('POST', $url . $this->url . '/delete/' . $id, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

        return $res;
    }

    /**
     * .033
     * @fecha: 15-12-2016
     * @parametros: $api_url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador del
     *     elemento a editar
     * @programador: Juan Bigorra /Pascual Madrid * @objetivo:Función para cambiar el valor de la columna active dado
     *     un identificador .
     */
    public function active( $url, $id ) {
        $res = $this->client->request('POST', $url . $this->url . '/active/' . $id, [ 'headers' => $this->headers, 'form_params' => [ 'api_token' => \Auth::user()->api_token, 'user_id' => \Auth::user()->id ] ]);

        return $res;
    }
}
