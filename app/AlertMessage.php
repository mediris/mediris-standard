<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class AlertMessage extends Model {


    /**
     * @fecha: 16-12-2016
     * @programador: Mercedes Rodriguez
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'message',
        'institution_id',
        'alert_message_type_id',
        'start_date',
        'end_date',
        'active',
    ];

    /**
     * @fecha: 09-02-2017
     * @programador: Mercedes Rodriguez
     * @objetivo: Relación: Un AlertMessage tiene un Institution.
     */
    public function institution() {
        return $this->belongsTo(Institution::class);
    }

    public function alertMessageType() {
        return $this->belongsTo(AlertMessageType::class);
    }

    //------------------------- FUNCIONES --------------------------------------------


    /**
     * @fecha: 09-02-2017
     * @programador: Mercedes Rodriguez
     * @objetivo: Obtener los mensajes de alerta que para el home
     */
    public static function getCurrentMessages() {
        $today = Carbon::now()->toDateTimeString();

        if(isset(Session::get('institution')->id)){

            $currentId = Session::get('institution')->id;

            $institutionsIds = [ 0, $currentId ];

            $alertMessages = self::where('start_date', '<=', $today)
                ->where('end_date', '>=', $today)
                ->where('active', '>=', 1)
                ->whereIn('institution_id', $institutionsIds)
                ->orderBy('id', 'ASC')
                ->with('alertMessageType')
                ->get([ 'id', 'message', 'alert_message_type_id' ]);

            if(empty(count($alertMessages))){

                $alertMessages = null;

            }
            
        }else{

            $alertMessages = null; 

        }     

        return ( $alertMessages );   
    }
    /**
     * @fecha: 13-07-2017
     * @programador: Ricardo Martos
     * @objetivo: contar la cantidad de mensajes que hay
     */


}
