<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class SuspendReason extends RemoteModel
{
    public function __construct() {
        $this->apibase = 'api/v1/suspend_reasons';
        parent::__construct();
    }
}