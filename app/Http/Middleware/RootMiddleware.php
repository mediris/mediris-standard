<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RootMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next ){
        if( Auth::user()->hasRole(1) || Auth::user()->id == 1){
            return $next($request);
        }else{
            //return redirect()->route('errors', [ 403 ]);
            abort(403);
        }
    }
}
