<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next ){
        if( Auth::user()->hasRole(1) || Auth::user()->id == 1 || Auth::user()->hasRole(99)){
            return $next($request);
        }else{
            //return redirect()->route('errors', [ 403 ]);
            abort(403);
        }
    }
}
