<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MedicalVisitorRequest extends Request{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $rules = [
            'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d\.,]+$/|required|max:50',
            'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d\.,]+$/|required|max:50',
            'administrative_ID' => 'required|max:10|unique_test:medical_visitors',
            'email' => 'required|email|max:255|unique_test:medical_visitors',
            'telephone_number' => 'min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number' => 'min:7|phone:US,VE,MOBILE',
            'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
            'medical_visitor_institution' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d\.,]+$/|required|max:250',
            'speciality' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d\.,]+$/|required|max:250',
        ];


        if( ! empty( $this->id ) ){
            $rules['administrative_ID'] = $rules['administrative_ID'] . ',' . $this->id;
            $rules['email'] = $rules['email'] . ',' . $this->id;
        }

        return $rules;
    }
}
