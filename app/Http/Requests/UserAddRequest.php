<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserAddRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'first_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/||required|max:25',
            'last_name' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'administrative_ID' => 'required|unique:users|max:45',
            'username' => 'alpha|required|max:25|unique:users',
            'password' => 'required|min:6|confirmed',
            'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number' => 'required|min:7|phone:US,VE,MOBILE',
            'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
            'cellphone_number_2' => 'min:7|phone:US,VE,MOBILE',
            'email' => 'required|email|max:255|unique:users',
            'prefix_id' => 'required',
            'suffix_id' => 'required',
        ];
    }
}
