<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitutionAddRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'name' => 'required|unique:institutions|max:50',
            'address' => 'required|max:250',
            'administrative_ID' => 'required|unique:institutions|max:45',
            'group_id' => 'required',
            'url' => 'required|unique:institutions|max:250|url',
            'email' => 'required|email|max:255|unique:institutions|regex:/^[\w._-]+@[a-zA-Z]+.[a-z]+.[a-z]*$/',
            'telephone_number' => 'required|min:7|phone:US,VE,FIXED_LINE',
            'telephone_number_2' => 'min:7|phone:US,VE,FIXED_LINE',
            'institution_id' => 'required|unique:institutions|max:45',
            'url_pacs' => 'required|max:250|url',
            'url_external_api' => 'max:250|url',
            'base_structure' => 'required|max:30',
        ];
    }
}
