<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PatientTypeAddRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25',
            'administrative_ID' => 'required|max:50|unique_test:patient_types',
            'priority' => 'required|numeric|min:1',
            'icon' => 'required',
            'parent_id' => 'required',
        ];
    }
}
