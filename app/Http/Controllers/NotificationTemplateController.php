<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\NotificationTemplate;
use JsValidator;


class NotificationTemplateController extends Controller {
    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';
    }

    public function rules( $unique = null ) {
        return [
            'description' => 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/|required|max:25|unique_test:notification_templates' . (($unique)?','.$unique:''),
            'template' => 'required'
        ];
    }

    /**
     * @fecha 22-05-2017
     * @programador Pascual Madrid
     * @objetivo Renderiza la vista index de la sección NotificationTemplates.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request ) {
        try {
            $templates = NotificationTemplate::remoteFindAll();

            return view('notificationTemplates.index', compact('templates'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "notificationTemplates", "index" );
        }
    }

    public function add( Request $request ) {        
        if( $request->isMethod('post') ) {
            $this->validate( $request, $this->rules() );
            
            try {
                $data = $request->all();
                $res = NotificationTemplate::remoteCreate( $data );

                if( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-add'));
                $request->session()->flash('class', 'alert alert-success');
            
                return redirect()->route('notification_templates');
            } catch( \Exception $e ) {
                return $this->logError( $e, "notification templates", "add");
            }
        } else {
            $validator = JsValidator::make( $this->rules(), array(), array(), "#TemplateAddForm" );
            $formattedFields = NotificationTemplate::$formatted_fields;
            return view('notificationTemplates.add', compact('validator', 'formattedFields'));
        }
    }

    public function edit( Request $request, $id ) {    
        if( $request->isMethod('post') ) {    
            $this->validate( $request, $this->rules( $id ) );
            
            try {
                $data = $request->all();
                $res = NotificationTemplate::remoteUpdate( $id, $data );

                if( isset( $res->error ) ){
                    return $this->parseFormErrors( $data, $res );
                }

                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');
            
                return redirect()->route('notification_templates');
            } catch( \Exception $e ) {
                return $this->logError( $e, "notification templates", "edit");
            }    
        } else {
            $template = NotificationTemplate::remoteFind( $id );
            $formattedFields = NotificationTemplate::$formatted_fields;
            $validator = JsValidator::make( $this->rules(), array(), array(), "#TemplateEditForm" );
            
            return view('notificationTemplates.edit', compact('template', 'validator', 'formattedFields'));
        }
    }

    public function active( Request $request, $id ){
        try {
            $res = NotificationTemplate::remoteToggleActive($id);

            if( isset( $res->error ) ){
                return $this->parseError( $res );
            }

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

            return redirect()->route('notification_templates');
        } catch( \Exception $e ) {
            return $this->logError( $e, "notification templates", "active");
        }
    }
}
