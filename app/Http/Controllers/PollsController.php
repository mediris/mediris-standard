<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Sex;
use App\polls;
use App\Patient;
use App\Result;
use App\Procedure;
use App\Modality2;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Log;
use Session;
use App\Notifications\NotificationPolls;

class PollsController extends Controller {
    public function __construct(){
        $this->polls = new polls();
        $this->url = 'api/v1/results';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }

    public function sendView(){
        try{

            $response = [
                'sexes'             => Sex::orderBy('name', 'asc')->get(),
                'modalities'        => Modality2::remoteFindAll( [ 'active' => 1 ] ),
                'procedures'        => Procedure::remoteFindByAction('/equipments' ),
                'first_day_date'    => self::getFirstDayDate(),
                'today_date'        =>self::getToday(),
            ];
            $sexos=[];
            foreach ($response['sexes'] as $sexo){
                $sexo->name=trans('labels.'.$sexo->name);
                array_push($sexos,$sexo);
    
            }
            $response['sexes']=$sexos;
            return view('polls.send',$response);

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: show');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }

    }

    public function sendPolls(Request $request){
        try{
            $conf = \App\Configuration::remoteFind(1, []);
            $activePolls = $conf->active_poll;
            
            if(!$activePolls){
                $request->session()->flash('message', trans('messages.error-polls-inactive'));
                $request->session()->flash('class', 'alert alert-danger');
                return self::sendView();
            }
            $fromDate = Carbon::createFromFormat('Y-m-d', $request->date_from);
            $toDate = Carbon::createFromFormat('Y-m-d', $request->date_to);
            $ageFrom = intval($request->age_from);
            $ageTo = intval($request->age_to);
            $sex = $request->sex;
            $modalities = $request->modalities;
            $patientsSent = [];

            $data = [
                "data_from"=>$fromDate->toDateTimeString(),
                "data_to"=>$toDate->toDateTimeString(),
                "sex"=>$sex,"modalities"=>$modalities
            ];
            $response = $this->polls->indexRemoteData($request->session()->get('institution')->url, $data);

            if ($response) {
                //get all patients ids into an array, and remove duplicates
                $patients_ids = [];
                foreach($response->data as $rp) {
                    if ( array_search($rp->service_request->patient_id, $patients_ids) === false ) {
                        $patients_ids[] = $rp->service_request->patient_id;
                    }
                }

                foreach($patients_ids as $idPatient) {
                    $patient = Patient::find($idPatient);
                    $send = true;
                    
                    //verify sex
                    if ( isset($data['sex']) && $patient->sex->id != $data['sex'] ) {
                        $send = false;    
                    }

                    //verify age
                    if ( $ageFrom || $ageTo ) {
                        $birthdate = new \DateTime($patient['birth_date']);
                        $birthdate = date_format($birthdate, 'Y-m-d');
                        $age =  Carbon::parse($birthdate)->age;
                        
                        if( $ageFrom && $age < $ageFrom ){
                            $send = false;
                        }

                        if( $ageTo && $age > $ageTo ){
                            $send = false;
                        } 
                    }

                    //send
                    if ( $send ) {
                        $patient->notify( new NotificationPolls( Session::get('institution')->id ) );
                        $patientsSent[] = $patient;
                    }
                }
            }
            if( count($patientsSent) == 0 ){
                $request->session()->flash('message', trans('messages.error-polls-not-coincidence'));
                $request->session()->flash('class', 'alert alert-danger');
                return self::sendView();
            } else {
                $modalitiesSerializer = serialize($request->modalities);
                $proceduresSerializer = serialize($request->procedure);
                $emailsSerializer = serialize( array_map( function( $item ) { return $item->email; } , $patientsSent) );
                
                $poll= new polls;
                $poll->email_to_send = $emailsSerializer;
                $poll->date_from = $request->date_from;
                $poll->date_to = $request->date_to;
                $poll->age_from = $request->age_from;
                $poll->age_to = $request->age_to;
                $poll->gender = $request->sex;
                $poll->modalities = $modalitiesSerializer;
                $poll->procedures = $proceduresSerializer;
                if($poll->save()){
                    $request->session()->flash('message', trans('alerts.email-sent'));  
                    $request->session()->flash('class', 'alert alert-success');
                    return self::sendView();   
                }
            }

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reports. Action: show');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
        
    }

    private function getFirstDayDate(){
        return date("Y-m-d", strtotime("first day of this month"));
    }

    private function getToday(){
        return date("Y-m-d", strtotime("today"));
    }

}
