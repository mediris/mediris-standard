<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Session;
use Illuminate\Support\MessageBag;
use Log;
use Activity;
use Auth;
use App\Search;
use App\Patient;
use App\RequestedProcedure;
use App\PatientType;
use App\Source;
use App\Referring;
use App\Answer;
use App\Modality2;
use App\Procedure;
use App\RequestedProcedureStatus;
use App\Category;
use App\SubCategory;
use App\BiRad;
use App\ServiceRequest;
use App\SuspendReason;
use App\Fix;


class FixController extends Controller
{

    public function __construct(){
        $this->logPath = '/logs/admin/admin.log';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request){

         return view('fix.index');
;
    }

    public function search(Request $request, $id){


}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id){
       
        try{

            $requestedProcedure = RequestedProcedure::remoteFind($id);

            if ( isset( $req->error ) ) {

                    return $this->parseFormErrors( $data, $req );

            } elseif ( $requestedProcedure->suspension_reason_id ) {
                $requestedProcedure->suspension_reason = SuspendReason::remoteFind( $requestedProcedure->suspension_reason_id );
            }
            $patient = $requestedProcedure->patient;
            $serviceRequest = $requestedProcedure->service_request;
            $patientTypes = PatientType::remoteFindAll();
            $sources = Source::remoteFindAll(['active' => 1]);
            $referrings = Referring::remoteFindAll(['active' => 1]);
            $answers = Answer::remoteFindAll(['active' => 1]);
            $modalities = Modality2::remoteFindAll(['active' => 1]);
            $procedures = Procedure::remoteFindAll(['active' => 1]);
            $categories = Category::remoteFindAll(['active' => 1]);
            $subcategories = SubCategory::remoteFindAll(['active' => 1]);
            $requestedProcedureStatus = new RequestedProcedureStatus();
            $requestedProcedureStatuses = $requestedProcedureStatus->getAll($request->session()->get('institution')->url);

            return view('fix.edit', compact( 'requestedProcedure',
                                                'patient',
                                                'serviceRequest',
                                                'patientTypes',
                                                'sources',
                                                'referrings',
                                                'answers',
                                                'requestedProcedureStatuses',
                                                'modalities',
                                                'procedures',
                                                'categories',
                                                'subcategories'
                                            ));
           
        }
        catch( \Exception $e ) {
            return $this->logError( $e, "search", "edit");
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request){


        if( $request->isMethod('post') ){
            $this->validate($request, [
                'patient_identification_id' => 'required|max:15',
            ]);

            $cedula = $request->all()['patient_identification_id'];

            $patient = Patient::where('patient_ID',$cedula)->get();

            if(count($patient) > 0){

               try{

                    $id = $request->all()['nroorden'];

                    $data = $request->only(['patient_identification_id', 'patient_id', 'nroorden']);  

                    $data['id']=$patient[0]['id'];

                    $data['first_name']=$patient[0]['first_name'];

                    $data['last_name']=$patient[0]['last_name'];

                    $data['sex_id']=$patient[0]['sex_id'];

                    $result= Fix::remoteUpdate( $id, $data);

                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('search');

                }catch( \Exception $e ){

                    $this->logError( $e, "patients", "edit");   
                } 
                

            }else{

                // return "El paciente no existe...";
                $request->session()->flash('message', 'El paciente no existe');
                $request->session()->flash('class', 'alert alert-danger');

                // return redirect()->route('search');

                return back();

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
   }
}

