<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\RequestedProcedure;
use App\ServiceRequest;
use App\Http\Requests;
use App\SubCategory;
use App\Radiologist;
use App\Institution;
use Dompdf\Dompdf;
use App\Category;
use App\Addendum;
use App\Patient;
use App\BiRad;
use Activity;
use Session;
use Log;
use App\User;

use Illuminate\Support\Facades\Validator;

use GuzzleHttp\Client;

use Carbon\Carbon;

class RadiologistController extends Controller {

    public function __construct(){
        $this->logPath = '/logs/radiologist/radiologist.log';

        $this->subcategory = new SubCategory();
    }

    /**
     * @fecha 28-11-2016
     * @programador Pascual Madrid / Juan Bigorra
     * @objetivo Renderiza la vista index de la sección Radiologist.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request, $status = null, $print = null ){
        try{
            if( $request->isMethod('post') ){
                if( $print == 1 ){
                    $fileName = $request->all()['filename'];
                    $contentType = $request->all()['contentType'];
                    $base64 = $request->all()['base64'];

                    $data = base64_decode($base64);

                    header('Content-Type:' . $contentType);
                    header('Content-Length:' . strlen($data));
                    header('Content-Disposition: attachment; filename=' . $fileName);

                } else {

                    $datos =  $request->all();

                    if (isset($datos['filter'])) {

                        array_push($datos['filter']['filters'], array('field' => 'institutionId','operator' => "eq","value" => session('institution')->id));

                    }else{

                        $datos['filter'] = array('logic' => 'and', 'filters' => array(array('field' => 'institutionId','operator' => 'eq','value' => session('institution')->id)));

                    }
                    $data = Radiologist::remoteIndexData( $datos, '/'.$status );

                    echo json_encode($data, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }

            return view('radiologist.index', [ 'status' => $status, 'radiologistsOrder' => $this->searchRadiologist()]);
        } catch( \Exception $e ) {
            return $this->logError( $e, "radiologist", "index");
        }
    }

    public function edit(Request $request, $id) {
        if ( $request->route() ) {
            $routeTokens = $request->route()->getCompiled()->getTokens();
        } else {
            $routeTokens[0][1] = '/';
        }

        if ( $routeTokens[0][1] == "/draft" ) {

            if ($request->isMethod('post')) {
                try {
                    $data['text'] = $request->input('text');
                    $res = RequestedProcedure::remoteUpdate( $id, $data );

                    if ( isset( $res->error ) ) {
                        return response()->json( $res );
                    } else {
                        return response()->json(['code' => '200', 'message' => trans('alerts.success-edit-draft'), 'id' => $res->id]);
                    }
                } catch (\Exception $e) {
                    return $this->logError( $e, "radiologist", "draft");
                }
            } else {
                $res = RequestedProcedure::remoteFind( $id );
                $pdf = \App\FinalReport::createReport( $res->formatReportData(), true )->getStream();
                return response($pdf, 200)->header('Content-Type', 'application/pdf');
            }

        } else {

            if ($request->isMethod('post')) {

                if ($request->get('approve') === "0")
                    $this->validate($request, [
                        'audio_data' => 'required_without:text',
                        'text' => 'required_without:audio_data',
                    ]);
                else
                    $this->validate($request, [
                        'text' => 'required'
                    ]);

                try {
                    $requestedProcedure = RequestedProcedure::remoteFind($id);

                    if($requestedProcedure['requested_procedure_status_id'] == 6 && $requestedProcedure['text'] != ''){
                        $request->session()->flash('message', trans('alerts.order-already-approved'));
                        $request->session()->flash('class', 'alert alert-danger');

                        return back();
                    }

                    $data = $requestedProcedure->toArray();
                    $data['radiologist_user_id'] = \Auth::user()->id; //Add the user_id who dictate
                    $data['radiologist_user_name'] = \Auth::user()->first_name . ' ' . \Auth::user()->last_name; //Add the name who dictate
                    $data['text'] = $request->input('text'); //Add the text of the template
                    $data['blocked_status'] = 0; //Unlock the order
                    $data['dictation_date'] = date("Y-m-d H:i:s");
                    $data['teaching_file'] = $request->input('teaching_file') == 1 ? 1 : 0;
                    $data['teaching_file_text'] = $request->input('teaching_file_text');
                    $data['category_id'] = $request->input('category_id');
                    $data['sub_category_id'] = $request->input('sub_category_id');
                    $data['bi_rad_id'] = $request->input('bi_rad_id');
                    $data['audio_data'] = $request->audio_data;
                    $data['observation_radiologist'] = $request->input('observation_radiologist');
                    $data['procedure']->templates = '';

                    if ($request->input('approve')) {
                        $res = RequestedProcedure::setAsFinished( $id, $data );
                    } else {
                        if (session()->all()['institution']->require_transcriber == 1) {
                            
                            $res = RequestedProcedure::setAsTranscribe( $id, $data );
                        }else{    
                            
                            $res = RequestedProcedure::setAsApprove( $id, $data );
                        }
                    }

                    if ( isset( $res->error ) ) {
                        return $this->parseFormErrors( $data, $res );
                    } else {
                        //create PDF
                        if ($request->input('approve')) {
                            \App\FinalReport::createReport( $res->formatReportData() )->toStorage();

                            if(session()->all()['institution']->url_external_api){
                                $url = session()->all()['institution']->url_external_api . "/reporte/" . session()->all()['institution']->administrative_id . '-' . $id .'-O';

                                $client = new Client();
                                $promise = $client->getAsync($url);
                                $promise->wait();

                                /*$ch = curl_init($url);
                                curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                                curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                                $result = curl_exec($ch);
                                curl_close($ch);*/
                            }
                        }

                        session()->flash('message', trans('alerts.success-edit'));
                        session()->flash('class', 'alert alert-success');

                        echo "<script>";
                        echo "alert('Orden aprobada con éxito!!!')";
                        echo "</script>";

                        echo "<script>";
                        echo "window.close()";
                        echo "</script>";

                        //return redirect()->route('radiologist');
                    }
                } catch (\Exception $e) {
                    return $this->logError( $e, "radiologist", "edit");
                }
            }

            try {
                $requestedProcedure = RequestedProcedure::remoteFind($id);
                $biRads = \App\BiRad::remoteFindAll();
                $categories = Category::remoteFindAll(['active' => 1]);
                $subcategories = SubCategory::remoteFindAll(['active' => 1]);
                $requestedProcedure->patient->age = $requestedProcedure->patient->getAgeAndMonth();
                // $requestedProcedure->lock('radiologist', 1);
                $requestedProcedure->lock('radiologist', 2);
                $patientIdPacs = $this->getPatientId($id);
                $pacs = $this->buildUrl($patientIdPacs, $id);

                return view('radiologist.edit', [
                                                    'requestedProcedure' => $requestedProcedure,
                                                    'biRads'            => $biRads,
                                                    'categories'        => $categories,
                                                    'subcategories'     => $subcategories,
                                                    'allowImpersonate'  => true,
                                                    'patientIdPacs'     => $patientIdPacs,
                                                    'pacs'              => $pacs,
                                                    'upacs'             => \Auth::user()->username_pacs,
                                                    'ppacs'             => \Auth::user()->password_pacs,
                                                    'invox'             => session()->all()['institution']->url_invox ? session()->all()['institution']->url_invox : 0,
                                                ]);
            } catch (\Exception $e) {
                return $this->logError( $e, "radiologist", "edit");
            }
        }
    }

    public function lock( Request $request, $id ){

        if( $request->isMethod('post') ){

            try {
                $requestedProcedure = RequestedProcedure::remoteFind($id);
                $res = $requestedProcedure->lock('radiologist', 2);

                return json_encode($res);
            } catch( \Exception $e ) {
                return $this->logError( $e, "radiologist", "lock");
            }
        }
    }

    public function addendum(Request $request, $id, $pdfversion=null) {
        if ($request->isMethod('POST')) {

            $this->validate($request, [
                    'text' => 'required',
                ]);
            try {
                $data = $request->all();
                $data['requested_procedure_id'] = $id;
                $res = Addendum::remoteCreate( $data );
                /**Para incluir el Logo en el PDF Inicio */
                $institutionId = session()->all()['institution']->id;
                $aditionalFields = Institution::select('report_header', 'report_footer')->where('id',$institutionId)->get();
                /**Para incluir el Logo en el PDF Fin */
                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {

                    if(session()->all()['institution']->url_external_api){
                        $url = session()->all()['institution']->url_external_api . "/reporte/" . session()->all()['institution']->administrative_id . '-' . $id .'-A';
                        
                        $client = new Client();
                        $promise = $client->getAsync($url);
                        $promise->wait();

                        /*$ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                        $result = curl_exec($ch);
                        curl_close($ch);*/
                    }

                    $requestedProcedure = RequestedProcedure::remoteFind( $res->requested_procedure->id );
                    $procedureFormat = $requestedProcedure->formatReportData( true );
                    $procedureFormat['header'] = 'storage/'.$aditionalFields[0]['attributes']['report_header'];
                    $procedureFormat['footer'] = 'storage/'.$aditionalFields[0]['attributes']['report_footer'];
                    //$procedureFormat['logoActives'] = $aditionalFields[0]['attributes']['logo_actives_report'];
                    \App\FinalReport::createAddendum($procedureFormat)->toStorage();
                    \App\FinalReport::createReport( $procedureFormat, true )->toStorage();
                    
                    session()->flash('message', trans('alerts.success-edit'));
                    session()->flash('class', 'alert alert-success');

                    echo "<script>";
                    echo "alert('Orden aprobada con éxito!!!')";
                    echo "</script>";

                    echo "<script>";
                    echo "window.close()";
                    echo "</script>";

                    //return redirect()->route('radiologist');
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "radiologist", "addendum");
            }
        }

        try {
            $requestedProcedure = RequestedProcedure::remoteFind($id);
            
            $serviceRequests = new Collection();

            $institution = $request->session()->get('institution');

            $serviceRequestsByInstitution = 
                $serviceRequests = ServiceRequest::remoteFindAll( [ 'patient_id' => $requestedProcedure->service_request->patient_id ] );

            $serviceRequestsByInstitution->institution_id = $institution->id;
            $serviceRequestsByInstitution->institution_name = $institution->name;
            $requestedProcedure->patient->age   = $requestedProcedure->patient->getAgeAndMonth();
            $serviceRequests->push( $serviceRequestsByInstitution );
            $patientIdPacs = $this->getPatientId($id);
            $pacs = $this->buildUrl($patientIdPacs, $id);
            $audio = $requestedProcedure->getDictationFile();

            return view('radiologist.addendum', [
                                                    'requestedProcedure'    => $requestedProcedure,
                                                    'serviceRequests'       => $serviceRequests,
                                                    'patientIdPacs'         => $patientIdPacs,
                                                    'pacs'                  => $pacs,
                                                    'audio'                 => $audio
                                                ]);
        } catch(\Exception $e) {
            return $this->logError( $e, "radiologist", "addendum");
        }
    }

    public function printer( Request $request, $status ){

        try{
            if( $status == 3 ){
                $requestedProcedures = Radiologist::getAllToDictate();
            }

            if( $status == 5 ){
                $requestedProcedures = Radiologist::getAllToApprove();
            }

            foreach( $requestedProcedures as $key => $requestedProcedure ){
                $requestedProcedures[$key]->patient = Patient::find($requestedProcedure->service_request->patient_id);
            }

            return view('radiologist.printer', compact('requestedProcedures'));
        }catch( \Exception $e ){
            return $this->logError( $e, "radiologist", "printer");
        }
    }

    public function approve( Request $request, $id ) {
        if( $request->isMethod('post') ){
            $this->validate($request, [
                'text' => 'required',
            ]);

            try {

                $requestedProcedure = RequestedProcedure::remoteFind($id);

                if($requestedProcedure['requested_procedure_status_id'] == 6 && $requestedProcedure['text'] != ''){
                    $request->session()->flash('message', trans('alerts.order-already-approved'));
                    $request->session()->flash('class', 'alert alert-danger');

                    return back();
                }

                $data = [];
                $data['text'] = $request->input('text');
                $data['blocking_user_id'] = 0;
                $institutionId = session()->all()['institution']->id;
                $aditionalFields = Institution::select('report_header', 'report_footer')->where('id',$institutionId)->get();

                if($request->input('category_id') != 1){
                    $data['category_id'] = $request->input('category_id');
                }

                if($request->input('sub_category_id') != 1){
                    $data['sub_category_id'] = $request->input('sub_category_id');
                }

                if($request->input('teaching_file_text')){
                    $data['teaching_file_text'] = $request->input('teaching_file_text');
                }

                if ($request->input('reject')) {
                    //Reject the transcription
                    $data['reject_reason'] = $request->input('reject_reason');
                    $data['reject_user_id'] = \Auth::user()->id; //Add the user_id who approve
                    $data['reject_user_name'] = \Auth::user()->first_name . ' ' . \Auth::user()->last_name; //Add the name who dictate
                    $data['reject_date'] = date("Y-m-d H:i:s");
                    
                    if (session()->all()['institution']->require_transcriber == 1) {

                        $res = RequestedProcedure::setAsTranscribe( $id, $data );   
                    }else{
                        $res = RequestedProcedure::setAsDictate( $id, $data, 1 );
                    }

                } else {
                    //Approve the transcription
            
                    if($request->input('user-change') == "" && Session::get('roles')[0]->id != 5){

                        $request->session()->flash('message', trans('alerts.user-radiologist'));
                        $request->session()->flash('class', 'alert alert-danger');

                        return back();
                    }

                    $res = RequestedProcedure::setAsFinished( $id, $data );
                }

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    if( !$request->input('reject') ) {
                        $procedureFormat = $res->formatReportData();

                        $procedureFormat['header'] = 'storage/'.$aditionalFields[0]['attributes']['report_header'];
                        $procedureFormat['footer'] = 'storage/'.$aditionalFields[0]['attributes']['report_footer'];
                        //$procedureFormat['logoActives'] = $aditionalFields[0]['attributes']['logo_actives_report'];

                        if(session()->all()['institution']->url_external_api){
                            $url = session()->all()['institution']->url_external_api . "/reporte/" . session()->all()['institution']->administrative_id . '-' . $id .'-O';
                            
                            $client = new Client();
                            $promise = $client->getAsync($url);
                            $promise->wait();

                            /*$ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            $result = curl_exec($ch);
                            curl_close($ch);*/
                        }

                        \App\FinalReport::createReport( $procedureFormat )->toStorage();
                    }
                }

                   echo "<script>";
                if (!$request->input('reject')) {

                    echo "alert('Orden aprobada con éxito!!!')";

                }else{   
                    echo "alert('Orden rechazada con éxito!!!')";
                }
                    echo "</script>";
                    echo "<script>";
                    echo "window.close()";
                    echo "</script>";

                //return redirect()->route('radiologist');
            } catch ( \Exception $e ) {
                return $this->logError( $e, "radiologist", "approve");
            }
        }

        try {
            $requestedProcedure = RequestedProcedure::remoteFind( $id );
            $audio = $requestedProcedure->getDictationFile();
            $biRads = \App\BiRad::remoteFindAll();
            $patientIdPacs = $this->getPatientId($id);
            $categories = Category::remoteFindAll(['active' => 1]);
            $subcategories = SubCategory::remoteFindAll(['active' => 1]);
            $pacs = $this->buildUrl($patientIdPacs, $id);
            
            return view('radiologist.approve', [
                                                    'requestedProcedure'=> $requestedProcedure,
                                                    'biRads'            => $biRads,
                                                    'audio'             => $audio,
                                                    'allowImpersonate'  => true,
                                                    'patientIdPacs'     => $patientIdPacs,
                                                    'categories'        => $categories,
                                                    'subcategories'     => $subcategories,
                                                    'pacs'              => $pacs,
                                                    'radiologistsOrder' => $this->searchRadiologist()
                                                ]);
        } catch( \Exception $e ) {
            return $this->logError( $e, "radiologist", "approve");
        }
    }

    public function revert( Request $request, $id ) {
        try {
            $data = [];
            $data['text'] = $request->input('text');
            $res = RequestedProcedure::setAsDictateFromRevert( $id, $data );

            if ( isset( $res->error ) ) {
                return response()->json( $res );
            } else {
                return response()->json(['code' => '200', 'message' => 'Updated', 'id' => $res->id]);
            }
        } catch( \Exception $e ) {
            return $this->logError( $e, "radiologist", "revert");
        }
    }

    /**
     * @fecha 31-07-2017
     * @programador Hendember Heras
     * @objetivo Comprueba si existe el url del PAC y los datos de usuario para hacer un redirect
     * @param Request $request
     * @return json response
     */
    public function pac( $id ) {
        $institution = Session::get('institution');
        $url = $institution->url_pacs;

        $user = \Auth::user();
        $uname = $user->username_pacs;
        $pass = $user->password_pacs;

        $message = "";

        if ( empty( $user ) ) {
            $ret = false;
            $message = trans('messages.no-pac-url');
        } elseif ( empty($uname) || empty($pass) ) {
            $ret = false;
            $message = trans('messages.no-pac-username');
        } else {
            $ret = true;

            /*//TO DO: hacer que el microsoft-edge sea configurable por usuario.
            $url = "microsoft-edge:" . $url;
            $url = $url . "/opalweb/IntegrationProcessor.aspx?CMD=OPENSTUDY&ACCESSION=#nroOrden#&u=#user#&p=#password";
            $url = str_replace("#user", $uname, $url);
            $url = str_replace("#password", $pass, $url);
            $url = str_replace("#nroOrden", $id, $url);

            //$url = "microsoft-edge:http://pacsmeditroncmdlt.dnsalias.com/opalweb/IntegrationProcessor.aspx?CMD=OPENSTUDY&ACCESSION=1039968&u=stecnico&p=@meditron8";*/
        }

        return response()->json([ 'success' => $ret, 'message' => $message, 'url' => $url ]);
    }

    /**
     * @fecha 20-11-2017
     * @programador Alton Bell Smythe
     * @objetivo Pseudo Routing for web.php with params and work like static
     */
    public function OrdenesDictar() {
        return redirect()->route('radiologist.status', [1]);
    }

    /**
     * @fecha 20-11-2017
     * @programador Alton Bell Smythe
     * @objetivo Pseudo Routing for web.php with params and work like static
     */
    public function OrdenesAprobar() {
        return redirect()->route('radiologist.status', [2]);
    }

    /**
     * @fecha 20-11-2017
     * @programador Alton Bell Smythe
     * @objetivo Pseudo Routing for web.php with params and work like static
     */
    public function ToAddendum() {
        return redirect()->route('radiologist.status', [3]);
    }

    /**
     * @fecha 20-11-2017
     * @programador Alton Bell Smythe
     * @objetivo Pseudo Routing for web.php with params and work like static
     */
    public function OrdenesDictadas() {
        return redirect()->route('radiologist.status', [4]);
    }

    public function assignOrder(Request $request){

        try{

            $requestedProcedureId =  $request->all()['orderId'];
            $radiologist = User::find($request->all()['radiologist']);

            $fullnameRadiologist =  $radiologist['first_name'] . " " . $radiologist['last_name'];

            \DB::table('apimeditron.requested_procedures')
              ->where('id', $request->all()['orderId'])
              ->update(['block_radiologist_status' => 1, 'blocking_user_id_radiologist' => $request->all()['radiologist'], 'blocking_user_name_radiologist' => $fullnameRadiologist]);

            return redirect()->route('radiologist');

        } catch( \Exception $e ) {
            return $this->logError( $e, "radiologist", "index");
        }
    }

    public function getPatientId($id){
        $pacs = session()->all()['institution']->url_pacs;

        $posDomain = strpos($pacs, 'exa');
        $domainTemp = substr($pacs, $posDomain);
        $ip = gethostbyname($domainTemp);

        $url = "http://".$ip.":9669/pid_query/?accession_no=".$id;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $patient_id = curl_exec($ch);

        curl_close($ch);

        return $patient_id == '0' ? '0' : $patient_id;
    }

    public function searchRadiologist(){
        
        $radiologists = \DB::table('charges')
                ->select('users.id', 'users.first_name', 'users.last_name')
                ->join('users', 'users.id', '=', 'charges.user_id')
                ->where('role_id', 5)
                ->where('institution_id', session()->all()['institution']->id)
                ->get();

            foreach($radiologists as $radiologist){
                $radiologist->fullname = $radiologist->first_name . " " . $radiologist->last_name;
            }
        return $radiologists;
    }

    public function buildUrl($patientid, $ordernum){
        
        $passphrase = $this->convertToHex(config('exa.sedes.'.session()->all()['institution']->administrative_id.'.passphrase'));
        $ivtemp = config('exa.sedes.'.session()->all()['institution']->administrative_id.'.iv');
        $integration = config('exa.sedes.'.session()->all()['institution']->administrative_id.'.integration_name');
        $key_code = config('exa.sedes.'.session()->all()['institution']->administrative_id.'.key_code');
        $urlpacs = session()->all()['institution']->url_pacs;
        $uname = \Auth::user()->username_pacs;
        $formatDate = $this->getDate();

        $key = hex2bin($passphrase);
        $iv = hex2bin($ivtemp);
        $message = "integration_type=".$integration."&generated_dt=".$formatDate."&username=".$uname."&account_no=".$patientid."&flag=1&accession_no=".$ordernum;
        $padMsg = $this->padString($message);

        $encrypted = $this->encrypt($padMsg,$key,$iv);

        return $urlpacs.'/secure_link/code/'.$key_code.'/payload/'.$encrypted;
    }

    public function convertToHex($str){
        return bin2hex($str);
    }

    public function getDate(){
        $today = str_replace(" ", "+", Carbon::now()->toDateTimeString());
        return str_replace(":", "%3A", $today);
    }

    public function padString($source) {

        $paddingChar = ' ';
        $size = 16;
        $padLength = $size - strlen($source);

        for ($i = 0; $i < $padLength; $i++) {
            $source .= $paddingChar;
        }

        return $source;
    }

    public function encrypt($message,$key,$iv){

        $encrypted = openssl_encrypt($message, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);

        return bin2hex($encrypted);
    }

    
}
