<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Database\Eloquent\Collection;
use App\PlatesSize;
use App\Referring;
use App\RequestedProcedure;
use App\ServiceRequest;
use App\Source;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\PreAdmission;
use DB;
use Illuminate\Support\MessageBag;
use Log;
use Activity;
use Auth;
use App\RequestStatus;
use App\PatientType;
use App\Answer;
use App\PregnancyStatus;
use App\SmokingStatus;
use App\Procedure;
use App\Sex;
use App\Suffix;
use App\Prefix;
use App\Responsable;
use App\Country;
use App\Modality2;
use App\PatientDocuments;
use App\Appointment;
use App\PatientState;
use App\Institution;

class PreAdmissionController extends Controller {

    public function __construct(){

        $this->requestedProcedure = new RequestedProcedure();
        $this->serviceRequest = new ServiceRequest();
        $this->user = new User();
        $this->responsable = new Responsable();
        $this->patientType = new PatientType();
        $this->pregnancyStatus = new PregnancyStatus();
        $this->smokingStatus = new SmokingStatus();
        $this->patientState = new PatientState();
    }


    /**
     * @fecha 28-11-2016
     * @programador Juan Bigorra
     * @objetivo Renderiza la vista index de la sección Pre-Admission.
     * @fecha 22-01-2018
     * @programador Carlos Yonusg
     * @inclusion de suspension de orden en el modulo de pre-registro.
     * @param Request $request
     * @param $print Identifica si se desea imprimir o renderizar la vista.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index( Request $request, $print = null ){

        try{

            if( $request->isMethod('POST') ){

                if( $print == 1 ){

                    $fileName = $request->all()['filename'];
                    $contentType = $request->all()['contentType'];
                    $base64 = $request->all()['base64'];

                    $data = base64_decode($base64);

                    header('Content-Type:' . $contentType);
                    header('Content-Length:' . strlen($data));
                    header('Content-Disposition: attachment; filename=' . $fileName);

                    echo $data;

                }else{
                    
                    $datos =  $request->all();

                    if (isset($datos['filter'])) {

                        array_push($datos['filter']['filters'], array("field" => "institutionId","operator" => "eq","value" => session('institution')->id));

                    }else{

                        $datos['filter'] = array('logic' => 'and', 'filters' => array(array('field' => 'institutionId','operator' => 'eq','value' => session('institution')->id)));

                    }

                    $data = PreAdmission::indexRemoteData( $datos );
                    
                    echo json_encode($data, JSON_UNESCAPED_UNICODE);
                    exit();
                }
            }
            $suspensionReasons = \App\SuspendReason::remoteFindAll(['active' => 1, 'admin_reason' => 1]);
            return view('preAdmission.index',compact('suspensionReasons'));

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/preadmission/preadmission.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: preadmission. Action: index');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function show( Request $request, $requested_procedure_id, $service_request_id = null ){

        try{
            $requestedProcedure = RequestedProcedure::remoteFind($requested_procedure_id);
            $patient = $requestedProcedure->patient;
            $patient->birth_date = substr($patient->birth_date, 0, 10);
            $patient->getAgeAndMonth();
            $serviceRequest = $requestedProcedure->service_request;
            $procedure = $requestedProcedure->procedure;

            $response = [
                'requestedProcedure'    => $requestedProcedure,
                'patient'               => $patient,
                'serviceRequest'        => $serviceRequest,
                'procedure'             => $procedure,
                'sexes'                 => Sex::orderBy('name', 'asc')->get(),
                'prefixes'              => Prefix::where('language', \App::getLocale())->orderBy('name', 'asc')->get(),
                'suffixes'              => Suffix::where('language', \App::getLocale())->orderBy('name', 'asc')->get(),
                'countries'             => Country::getCountryByLocale(),
                'patientTypes'          => PatientType::remoteFindAll(['active' => 1]),
                'sources'               => Source::remoteFindAll(['active' => 1]),
                'referrings'            => Referring::remoteFindAll(['active' => 1]),
                'answers'               => Answer::remoteFindAll(['active' => 1]),
                'pregnancyStatuses'     => $this->pregnancyStatus->getAll($request->session()->get('institution')->url),
                'smokingStatuses'       => $this->smokingStatus->getAll($request->session()->get('institution')->url),
                'patientStates'         => $this->patientState->getAll($request->session()->get('institution')->url),
                'modalities'            => Modality2::remoteFind($requestedProcedure->procedure->modality_id),
                'procedures'            => Procedure::remoteFindAll(['active' => 1])
            ];

            return view('preAdmission.show', $response);

        }catch( \Exception $e ){

            Log::useFiles(storage_path() . '/logs/preadmission/preadmission.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: preadmission. Action: edit');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }

    public function approve( Request $request, $requested_procedure_id ){

        try{

            if( $request->isMethod('POST') ) {
                $rp = RequestedProcedure::remoteFind( $requested_procedure_id );
                $sr = ServiceRequest::remoteFind( $rp->service_request_id );
                
                foreach( $sr->requested_procedures as $rp ) {

                    $technician = Procedure::remoteFind($rp->procedure_id);
                    
                    if ($technician['technician'] == 1) {
                        
                        $res = RequestedProcedure::setAsToDo( $rp->id );

                    }else{

                        $res = RequestedProcedure::setAsFinished( $rp->id, [], false );
                    }

                    if ( isset( $res->error ) ) {
                        return $this->parseFormErrors( [], $res );
                    }
                }     
                
                session()->flash('message', trans('alerts.success-add'));
                session()->flash('class', 'alert alert-success');

                return response()->json([ 'code' => '200', 'message' => trans('labels.order-approved'), 'rp' => $res ]);
            }

            return true;

        }catch( \Exception $e ){
            Log::useFiles(storage_path() . '/logs/preadmission/preadmission.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: preadmission. Action: edit');

            return view('errors.index', [ 'code' => $e->getCode(), 'message' => $e->getMessage(), 'file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTraceAsString() ]);
        }
    }



/**
 * @Fecha: 22/01/18
 * @Programador: Carlos Yonusg
 * @Objetivo: Inclusion de suspension de ordenes en el modulo de pre-registro
 */
	public function suspend( Request $request ) {
		try{
			$data['suspension_reason_id'] 	= $request->suspensionReason;
			$res = RequestedProcedure::setAsSuspended( $request->orderId, $data );

			$res->requested_procedure_status->description = trans('labels.' . $res->requested_procedure_status->description);
			return json_encode($res);
		} catch( \Exception $e ) {
			return $this->logError( $e, "preadmission", "suspend");
		}

	}

	public function lock( Request $request, Patient $patient, $service_request_id = NULL ){

		try{
			if( $request->isMethod('POST') ){
				//$oldValues = [ 'patient' => [], 'serviceRequest' => [] ];
				$newValues = [ 'patient' => [], 'serviceRequest' => [] ];

				if( isset( $patient ) ) {
					//array_push($oldValues['patient'], $patient->getOriginal());
					
					$updatedPatient = $patient->lock();
					array_push($newValues['patient'], $updatedPatient);

					if ( isset( $service_request_id ) ) {
						$response = ServiceRequest::lock($service_request_id);

						if ( isset( $response->error ) ) {
							return $this->parseFormErrors( [], $res );
						} else {
							//array_push($oldValues['serviceRequest'], (array)$response->oldValue);
							array_push($newValues['serviceRequest'], (array)$response->newValue);	
						} 
					}
				}

				return json_encode([ 'code' => 200, $newValues ]);
			}
		} catch( \Exception $e ) {
			return $this->logError( $e, "preadmission", "lock");
		}
	}

    public function printer(){

        return true;
    }
}
