<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;
use App\Validation;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('unique_test', function($attribute, $value, $parameters)
        {
            $table = $parameters[0];
            if(isset($parameters[1]))
            {
                $id= $parameters[1];
            }
            else
            {
                $id = null;
            }

            $validation = new Validation();

            $response = !$validation->unique(Session::get('institution')->url, $table, $attribute, $value, $id);
            $response = $response == 0 ? false : true;
            return $response;
        }, trans('validation.unique'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
