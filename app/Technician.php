<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;


class Technician extends RemoteModel {

    public function __construct() {
        $this->apibase = 'api/v1/technician';
        parent::__construct();
    }
}