<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;


class Radiologist extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/radiologist';
        parent::__construct();
    }

    /**
     * @param $url
     * @return Collection
     */
    public static function getAllToDictate() {
        $requestedProcedures = parent::remoteFindByAction( '/datasource/3' );

        return $requestedProcedures;
    }

    /**
     * @param $url
     * @return Collection
     */
    public static function getAllToApprove() {
        $requestedProcedures = parent::remoteFindByAction( '/datasource/5' );

        return $requestedProcedures;
    }
}
