<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class Deliver extends Model {
    public function __construct() {
        $this->client = new Client();
        $this->url = 'api/v1/delivers';
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para obtener una colección de Delivers desde el api dado un identificador de
     *     RequestedProcedure.
     */
    public function getByOrder( $url, $id ) {
        $response = $this->client->request('POST', $url . $this->url . '/' . $id, [ 'headers' => $this->headers, 'form_params' => [ 'user_id' => Auth::user()->id, 'api_token' => Auth::user()->api_token ] ]);

        $delivers = new Collection();

        foreach ( json_decode($response->getBody()) as $element ) {
            $deliver = new Deliver();
            foreach ( $element as $key => $value ) {
                $deliver->$key = $value;
            }
            $delivers->push($deliver);
        }

        return $delivers;
    }

    /**
     * @fecha: 15-12-2016
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $data = Los datos a ser
     *     guardados
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para crear una nueva instancia de Deliver.
     */
    public function add( $url, $data ) {
        $res = $this->client->request('POST', $url . $this->url . '/add', [ 'headers' => $this->headers, 'form_params' => $data ]);

        return $res;
    }
}
