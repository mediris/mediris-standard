<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */
    'done-orders' => 'Done orders report',
    'dictated-orders' => 'Dictated orders report',
    'approved-orders' => 'Approved orders report',
    'transcribed-orders' => 'Transcribed orders report',
    'orders-list' => 'Orders list',
    'appointments' => 'Appointments report report',
    'radiologist-fees' => 'Radiologist fees report',
    'technician-fees' => 'Technician fees report',
    'transcriber-fees' => 'Transcriber fees report',
    'nulled-requisitions' => 'Nulled requisitions report',
    'birads' => 'BIRADS report',
    'patients-by-genre' => 'Patients by genre',
    'orders-to-dictat' => 'Orders to dictate',
    'orders-to-approve' => 'Order to approve',
    'orders-to-transcribe' => 'Orders to transcribe',
    'eficiency-indicator' => 'Eficiency indicator',
    'transcription' => 'Transcription report',


];