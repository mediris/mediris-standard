<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Statuses Language Lines
    |--------------------------------------------------------------------------
    |
    | .
    |
    */

    'doctor' => 'by a doctor',
    'clinic' => 'by a clinic',
    'friends' => 'by friends',
    'patient' => 'by other patient',
    'radio' => 'via radio',
    'press' => 'via press',
    'magazines' => 'by magazines',
    'tv' => 'via tv',
    'cinema' => 'cinema',
    'internet' => 'internet',
    'social' => 'social media',
    'other' => 'others',

    'not-pregnant' => 'Not pregnant',
    'possibly-pregnant' => 'Possibly pregnant',
    'definitely-pregnant' => 'Definitely pregnant',
    'unknown' => 'Unknown',
    'na' => 'N/A',

    'yes' => 'yes',
    'no' => 'no',


];