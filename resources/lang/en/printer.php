<?php

return [

    //::::::::::::::: ADDENDUMS PDF TRANSLATIONS :::::::::::::::
    'addendum-order' => 'addendum_order_',
    'final-order' => 'order_',
    'report' => 'Report',
    'order' => 'Order',
    'order-no' => 'Order #',
     'preview_order' => 'preview_order_',

];