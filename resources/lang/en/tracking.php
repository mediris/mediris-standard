<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Tracking Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the user tracking action.
    |
    */

    'create' => ':section with id: :id was created.',
    'show' => ':section with id: :id was showed.',
    'edit' => ":section with id: :id was edited. 
                Old value: :oldValue
                New value: :newValue",
    'delete' => ':section with id: :id was deleted.',

    'attempt' => 'attempt to :action :section without success',

    'attempt-edit' => 'attempt to :action :section with id: :id without success',

    'create-api' => ':section with id: :id was created. in the institution with id: :id-institution',

    'attempt-api' => 'attempt to :action :section in the institution with id: :id-institution without success',

    'edit-api' => ":section with id: :id was edited in the institution with id: :id-institution. 
                Old value: :oldValue
                New value: :newValue",

    'delete-api' => ':section with id: :id in the institution with id: :id-institution was deleted.',

    'change-password' => ':section with id :id change his password',

    'password-attempt' => ':section with id :id attempt to change the password',

    'rewrite-api' => ':section with id: :id in the institution with id: :id-institution was wrote.',

];