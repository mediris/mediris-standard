<?php

return [

	/*::::::::::::::::::::::::::: SERVICE REQUEST (RECEPTION) :::::::::::::: ::::::::::::::::::::::::::*/
	'responsable-id'                    => 'ID of the representative in case you are a minor',
	'referring'                    => 'Dr/Institution that refers the patient',
	'search'                    => 'Fill out any of the fields and press the Search button',
	'instructions'                    => 'instructions',
	'modality'                    => 'Ultrasound(US), Mammography(MG), X-rays(XR/CR/DR) ,Tomography(CT), Densitometry (OT), Gammacamara (XA), Resonance (MR), Nuclear medicine (NM), X-ray Fluoroscopy (RF)...',
	'observations_service_request' => 'observations of the application, these will detail some particularity inherent to the patient',
	'observations_procedure' => 'observations of the order, these will detail some particularity of it',
];