<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */

    'success-add' => 'Data successfully added',
    'success-edit' => 'Data successfully edited',
    'success-delete' => 'Data successfully deleted',
    'error-add' => 'Data not added',
    'error-edit' => 'Data not edited',
    'confirm-delete-row' => 'Are you sure to delete this row and all related data?',
    '23000' => 'Cannnot delete or update a parent row',
    'success-edit-password' => 'The password was successfully changed',
    'error-edit-password' => 'The actual password provided does not match with the registers',
    'select-valid-step' => 'Please select a valid step',
    'select-valid-procedure' => 'Please select a valid procedure',
    'forbidden' => 'The requested operation is forbidden and cannot be completed.',
    'block-unavailable' => 'The requested blocks are unavailables. Please try another one',
    'add-block' => 'Cannot add appointments on this view',
    'delete-block' => 'This block cannot be removed',
    'move-block' => 'This block cannot be moved',
    'edit-block' => 'This block can not be edited',
    'select-procedure' => 'Please select a correct procedure',
    'insufficient-block' => 'Not enough time for the selected blocks. Please choose another.',
    'busy-block' => 'The selected block are busy. Please choose another.',
    'appointment-created' => 'The appointment was already scheduled',
    'appointment-edited' => 'The appointment was already edited',
    'all-required' => 'All the fields are required.',
    'wrong-date' => 'Cannot add the appointment to the selected block. Please select other when the date are greater tha actual date',
    'appointment-discharged' => 'The appointment was already canceled',
    'reason-to-delete' => 'Please enter the reason to delete this appointment',
    'wrong-reason' => 'Please enter a valid reason',
    'see-record' => 'See his record',
    'provide-criteria' => 'Please provide a search criteria.',
    'select-valid-template' => 'Please select a valid template.',
    'confirm-template' => 'Are you sure to insert the selected template? The changes made will be rewritten.',
    'success-transcribe-order' => 'Order successfully transcribed',
    'no-data' => 'The report is empty',
    'success-edit-draft' => 'Draft successfully saved',
    'no-reverse' => "This change can't be undone, are you shure you want to continue?",
    'wrong-room-hour' => "The Room's end Hour can't be before the Start hour",
    

    /*You watching this,
    Insert your new message in alphabetical order below this line, also, pick one of the messages above and insert it in the already ordered list.
    Together we can make this a better place.
    */
    'confirm-action'=>'Are you sure you want to continue?',
    'email-sent' => 'the email was sent',
    'error-test-conn'=>'Failed test connection',
    'field' => 'The field',
    'field-required' => 'is requered',
    'invalid-test-conn'=>'Invalid connection data',
    'no-email' => 'The patient does not have assigned email',
    'no-logo-email'=>'The Institution does not have an assigned email logo',
    'no-result-email' => 'The configuration "Send report" isn\'t enabled',
    'room-not-ready' => "The room data isn't complete, please contact the system administrator",
    'same-procedure' => "you can not select the same procedure more than once",
    'success-test-conn'=>'Successfully test connection',
    'user-cancel-action'=>'Action canceled by user',

    // kill session - JCH
    'confirm'=>'Confirmation',
    'kill-session'=>'These changes will force the disconnection of all users',
    'user-radiologist'=>'You must select a radiologist',
    'user-technician'=>'You must select a technician or a radiologist',

    'order-already-approved' => 'This order has already been approved and the report cannot be modified'
];