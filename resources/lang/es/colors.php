<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Colors Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */

    '1' => '#0079c2',
    '2' => '#a94442',
    '3' => '#203d4f',
    '4' => '#00a2d9',

];