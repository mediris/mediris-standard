<?php

return [

	/*::::::::::::::::::::::::::: SERVICE REQUEST (RECEPTION) :::::::::::::: ::::::::::::::::::::::::::*/
	'responsable-id'                    => 'cédula del representante en caso de que sea menor de edad',
	'referring'                    => 'Dr/Institucion que refiere al paciente',
	'search'                    => 'complete alguno de los campos y presione el botón Buscar',
	'instructions'                    => 'instrucciones',
	'modality'                    => 'Ultrasonido (US), Mamografía (MG), Rayos X (XR/CR/DR), Tomografía (CT), Densitometría (OT), Gammacamara (XA), Resonancia (MR), Medicina Nuclear (NM), Rayos X Fluoroscopia (RF)...',
	'observations_service_request' => 'observaciones de la solicitud, estas detallaran alguna particularidad inherente al paciente',
	'observations_procedure' => 'observaciones de la orden, estas detallaran alguna particularidad de la misma',
];