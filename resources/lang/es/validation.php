<?php
return array(
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used
    | by the validator class. Some of the rules contain multiple versions,
    | such as the size (max, min, between) rules. These versions are used
    | for different input types such as strings and files.
    |
    | These language lines may be easily changed to provide custom error
    | messages in your application. Error messages for custom validation
    | rules may also be added to this file.
    |
    */
    "accepted"       => "El campo :attribute debe ser aceptado.",
    "active_url"     => "El campo :attribute no es una URL válida.",
    "after"          => "El campo :attribute debe ser una fecha después de :date.",
    "alpha"          => "El campo :attribute sólo puede contener letras.",
    "alpha_dash"     => "El campo :attribute sólo puede contener letras, números y guiones.",
    "alpha_num"      => "El campo :attribute sólo puede contener letras y números.",
    "array"          => "El campo :attribute debe ser un arreglo.",
    "before"         => "El campo :attribute debe ser una fecha antes :date.",
    "between"        => array(
        "numeric" => "El campo :attribute debe estar entre :min - :max.",
        "file"    => "El campo :attribute debe estar entre :min - :max kilobytes.",
        "string"  => "El campo :attribute debe estar entre :min - :max caracteres.",
        "array"   => "El campo :attribute debe tener entre :min y :max elementos.",
    ),
    "boolean"        => "El campo :attribute debe ser verdadero o falso.",
    "confirmed"      => "El campo de confirmación de :attribute no coincide.",
    "date"           => "El campo :attribute no es una fecha válida.",
    "date_format" 	 => "El campo :attribute no corresponde con el formato :format.",
    "different"      => "Los campos :attribute y :other deben ser diferentes.",
    "digits"         => "El campo :attribute debe ser de :digits dígitos.",
    "digits_between" => "El campo :attribute debe tener entre :min y :max dígitos.",
    "email"          => "El formato del :attribute es inválido.",
    "exists"         => "El campo :attribute seleccionado es inválido.",
    "filled"         => 'El campo :attribute es requerido.',
    "image"          => "El campo :attribute debe ser una imagen.",
    "in"             => "El campo :attribute seleccionado es inválido.",
    "integer"        => "El campo :attribute debe ser un entero.",
    "ip"             => "El campo :attribute debe ser una dirección IP válida.",
    "json"           => "El campo :attribute debe ser una cadena JSON válida.",
    "match"          => "El formato :attribute es inválido.",
    "max"            => array(
        "numeric" => "El campo :attribute debe ser menor que :max.",
        "file"    => "El campo :attribute debe ser menor que :max kilobytes.",
        "string"  => "El campo :attribute debe ser menor que :max caracteres.",
        "array"   => "El campo :attribute debe tener al menos :min elementos.",
    ),
    "mimes"         => "El campo :attribute debe ser un archivo de tipo :values.",
    "min"           => array(
        "numeric" => "El campo :attribute debe tener al menos :min.",
        "file"    => "El campo :attribute debe tener al menos :min kilobytes.",
        "string"  => "El campo :attribute debe tener al menos :min caracteres.",
    ),
    "not_in"                => "El campo :attribute seleccionado es invalido.",
    "numeric"               => "El campo :attribute debe ser un número.",
    "regex"                 => "El formato del campo :attribute es inválido.",
    "required"              => "El campo :attribute es requerido.",
    "required_if"           => "El campo :attribute es requerido cuando el campo :other es :value.",
    "required_unless"       => 'El campo :attribute es requerido a menos que :other esté presente en :values.',
    "required_with"         => "El campo :attribute es requerido cuando :values está presente.",
    "required_with_all"     => "El campo :attribute es requerido cuando :values está presente.",
    "required_without"      => "El campo :attribute es requerido cuando :values no está presente.",
    "required_without_all"  => "El campo :attribute es requerido cuando ningún :values está presente.",
    "same"                  => "El campo :attribute y :other debe coincidir.",
    "size"                  => array(
        "numeric" => "El campo :attribute debe ser :size.",
        "file"    => "El campo :attribute debe tener :size kilobytes.",
        "string"  => "El campo :attribute debe tener :size caracteres.",
        "array"   => "El campo :attribute debe contener :size elementos.",
    ),
    "string"               => "El campo :attribute debe ser una cadena.",
    "unique"         => "El campo :attribute ya ha sido tomado.",
    "url"            => "El formato de :attribute es inválido.",
    "timezone"       => "El campo :attribute debe ser una zona válida.",
    "phone" => "El campo :attribute contiene un formato inválido.",
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute_rule" to name the lines. This helps keep your
    | custom validation clean and tidy.
    |
    | So, say you want to use a custom validation message when validating that
    | the "email" attribute is unique. Just add "email_unique" to this array
    | with your custom message. The Validator will handle the rest!
    |
    */
    'custom' => array(
        'attribute-name' => array(
            'rule-name'  => 'custom-message',
        ),
        'procedures' => array(
            'required' => 'Debe agregar al menos un procedimiento', 
        ),
        'start_hour' => array(
            'date_format' => 'El campo hora de inicio debe ser de la forma 06:30 AM',
        ),
        'end_hour' => array(
            'date_format' => 'El campo hora de fin debe ser de la forma 05:30 PM',
        ),
        'patient_allergies' => array(
            'required_if' => 'El campo Alergias es requerido',
        ),
        'patient_weight' => array(
            'required_if' => 'Peso es requerido'
        ),
        'patient_height' => array(
            'required_if' => 'Estatura es requerido'
        ),
        'patient_abdominal_circumference' => array(
            'required_if' => 'Circunferencia abdominal es requerido'
        ),
    ),
    /*
    |--------------------------------------------------------------------------
    | Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". Your users will thank you.
    |
    | The Validator class will automatically search this array of lines it
    | is attempting to replace the :attribute place-holder in messages.
    | It's pretty slick. We think you'll like it.
    |
    */
    'attributes' => [
        'username' => 'nombre de usuario',
        'password' => 'contraseña',
        'name' => 'nombre',
        'section_id' => 'sección',
        'description' => 'descripción',
        'units' => 'unidades',
        'ae_title' => 'título',
        'room_id' => 'sala',
        'modality_id' => 'modalidad',
        'administrative_id' => 'Id administrativo',
        'administrative_ID' => 'Id administrativo',
        'group_id' => 'grupo',
        'address' => 'dirección',
        'occupation' => 'ocupación',
        'country_of_residence' => 'país de residencia',
        'country_id' => 'país de residencia',
        'telephone_number' => 'número telefónico',
        'telephone_number_2' => 'número telefónico secundario',
        'cellphone_number' => 'número de celular',
        'cellphone_number_2' => 'Número de celular secundario',
        'email' => 'correo electrónico',
        'patient_id' => 'Id paciente',
        'first_name' => 'nombre',
        'last_name' => 'apellido',
        'name_prefix' => 'prefijo',
        'citizenship' => 'ciudadania',
        'sex_id' => 'genero',
        'birth_date' => 'fecha de nacimiento',
        'patient_ID' => 'Id paciente',
        'history_id' => 'Nro de historia',
        'priority' => 'prioridad',
        'icon' => 'ícono',
        'division_id' => 'división',
        'parent_id' => 'padre',
        'indications' => 'indicaciones',
        'actual_password' => 'contraseña actual',
        'referring_id' => 'referente',
        'steps' => [
            '*' => [
                'description' => 'descripción del paso',
                'administrative_ID' => 'Id administrativo del paso',
                'modality_id' => 'modalidad del paso',
                'indications' => 'indicaciones del paso',
                'order' => 'orden del paso',
            ],
        ],
        'procedure_id' => 'procedimiento',
        'prefix_id' => 'prefijo',
        'requestedProcedures' => 'procedimientos',
        'unit_id' => 'unidades',
        'birthday_expression' => 'CRON expression cumpleaños',
        'mammography_expression' => 'CRON expression mamografía',
        'appointment_sms_expression' => 'CRON expression Citas',
        'appointment_sms_notification' => 'frecuencia de cita vía sms',
        'weight' => 'peso',
        'height' => 'estatura',
        'patient_type_id' => 'tipo de paciente',
        'source_id' => 'procedencia',
        'answer_id' => '¿cómo supo de nosotros?',
        'smoking_status_id' => 'fumador',
        'pregnancy_status_id' => 'estado de embarazo',
        'patient_state' => 'estado del paciente',
        'issue_date' => 'fecha de creación',
        'patient_identification_id' => 'Cédula de identidad',
        'patient' => [
            'first_name' => 'nombre',
            'sex_id' => 'género',
            'last_name' => 'apellido',
            'birth_date' => 'fecha de nacimiento',
            'name_prefix' => 'prefijo',
            'country_id' => 'país',
            'citizenship' => 'ciudadania',
            'occupation' => 'ocupación',
            'address' => 'dirección',
        ],
        'text' => 'texto',
        'patient_email' => 'correo electrónico',
        'patient_telephone_number' => 'número telefónico',
        'patient_cellphone_number' => 'número de celular',

        'radiologist_fee' => 'honorarios del radiólogo',
        'technician_fee' => 'honorarios del técnico',
        'transcriptor_fee' => 'honorarios del transcriptor',

        'alert_message_type_id' => 'tipo de mensaje de alerta',
        'start_date' => 'inicio',
        'end_date' => 'fin',
        'message' => 'mensaje',

        'block_size' => 'tamaño del bloque',
        'quota' => 'cupos por bloque',
        'equipment_number' => 'número de equipos',

        'serviceRequest' => [
            'weight' => 'peso',
            'height' => 'estatura',
            'smoking_status_id' => 'Fumador',

        ],

        'requestedProcedure' => [
            'number_of_plates' => 'No. Placas',
            'equipment_id'     => 'Equipo'
        ],
        'endDate' => 'Fecha Fin',
        'startDate' => 'Fecha Inicio',
        'modality' => 'Modalidades',
        'procedure' => 'Procedimiento',
        'patientType' => 'Tipo de Paciente',
        'requestedProcedureStatus' => 'Estatus',
        'suspensionReason' => 'razón de suspensión',
	    'template' => 'Plantilla',
        'base_structure' => 'estructura base',
        'modalities' => 'Modalidades',
        'appointmentStatus' => 'Estatus',
        'users' => 'Usuarios',
        'rooms' => 'Salas',

        //Orden alfabético por favor. Gracias 
        'patient_allergies_check' => 'Alergias',
        'patient_first_name' => 'Nombre',
        'patient_last_name' => 'Apellido',
        'procedure_contrast_study' => 'Estudio con contraste',
        'referring' => 'Referente',

        'referring_institution' => 'institución',
        'speciality' => 'especialidad',
    ],
);
