
<div class="container-fluid">

    <form method="post" action="{{ route('reports.show', $fields['id']) }}" id="ReportGenerateForm">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.generate')).' '.trans('labels.reports') }}</h4>
            </div>
        </div>

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach

                    </ul>
                </div>
            @endif
        </div>

        @foreach($fields as $field => $values)
            @if($field != "id")
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            @if($field != 'endToday' && $field != 'startToday')
                                <label for="{{ $field }}">{{ ucfirst(trans('labels.'.$field)) }}</label>
                            @endif
                        </div>
                        <div>
                            @if(is_array($values) || is_object($values))
                                <select name="{{ $field }}[]" id="{{ $field }}" class="form-control" multiple="multiple">
                                    @foreach($values as $value)
                                        @if($value->description != null)
                                            @if( $field == 'requestedProcedureStatus' || $field == 'appointmentStatus')
                                                <option value="{{ $value->id }}" selected>{{ ucfirst(trans('labels.'.$value->description)) }}</option>
                                            @else
                                                <option value="{{ $value->id }}" selected>{{ ucfirst($value->description) }}</option>
                                            @endif
                                        @elseif($value->name != null)
                                            <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                        @elseif($value->first_name != null)
                                            <option value="{{ $value->id }}" selected>{{ $value->first_name . ' ' . $value->last_name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            @else
                                @if($values == 'date')
                                    @if($field == 'endDate')
                                        <input type="{{ $values }}" name="{{ $field }}" id="{{ $field }}" class="form-control" value="{{ $fields['endToday'] }}"/>
                                    @else
                                        <input type="{{ $values }}" name="{{ $field }}" id="{{ $field }}" class="form-control" value="{{ $fields['startToday'] }}"/>
                                    @endif
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        @endforeach

        <div class="row">
            <div class="modal-footer">
                <input id='btn-csv' class="btn btn-form" type='submit' name='button' data-style="expand-left" value='{{ ucfirst(trans('labels.csv')) }}'>
                <input id='btn-pdf' class="btn btn-form" type='submit' name='button' data-style="expand-left" value='{{ ucfirst(trans('labels.pdf')) }}'>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\ReportRequest', '#ReportGenerateForm'); !!}
</div>

<script>
    closeColorBox();  //script que permite cerrar el modal donde se aplican los diferentes filtros para la generacion del reporte
    loadDatePickerReport();
    ColorBoxmultiSelectsInit();
</script>