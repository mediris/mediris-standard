@if(Auth::user()->hasRole(1))
    <li class="{{ (Request::segment(1) == 'configurations' ||  Request::segment(1) == 'consumables' || Request::segment(1) == 'divisions' ||
                                Request::segment(1) == 'equipment' || Request::segment(1) == 'patientTypes' ||
                                Request::segment(1) == 'alertMessages' || Request::segment(1) == 'procedures' ||
                                Request::segment(1) == 'rooms' || Request::segment(1) == 'steps' ||
                                Request::segment(1) == 'units' || Request::segment(1) == 'notification_templates' ||
                                Request::segment(1) == 'suspend_reasons')? 'current active' : ''}}">
        <a href="#" target="_self">
            <i class="ico icon-configuracion"></i>
            <span class="sidebar-text ">{{ ucfirst(trans('labels.configuration')) }}</span>
            <span class="arrow glyphicon glyphicon-triangle-right
                                {{ (Request::segment(1) == 'configurations' ||  Request::segment(1) == 'consumables' || Request::segment(1) == 'divisions' ||
                                Request::segment(1) == 'equipment' || Request::segment(1) == 'patientTypes' ||
                                Request::segment(1) == 'alertMessages' || Request::segment(1) == 'procedures' ||
                                Request::segment(1) == 'rooms' || Request::segment(1) == 'steps' ||
                                Request::segment(1) == 'units' || Request::segment(1) == 'notification_templates' ||
                                Request::segment(1) == 'suspend_reasons')? 'arrow-down' : ''}}" id="arrow-sistem">
                                </span>
        </a>
        <ul class="submenu collapse">
            @include('partials.submenus.configurations')
            <li><a href="{{ route('consumables') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.consumables')) }}</span></a></li>
            <li><a href="{{ route('divisions') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.divisions')) }}</span></a></li>
            <li><a href="{{ route('equipment') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.equipment')) }}</span></a></li>
            <li><a href="{{ route('patientTypes') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.patientTypes')) }}</span></a></li>
            <li><a href="{{ route('alertMessages') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.alertMessages')) }}</span></a></li>
            <li><a href="{{ route('procedures') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.procedures')) }}</span></a></li>
            <li><a href="{{ route('rooms') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.rooms')) }}</span></a></li>
            <li><a href="{{ route('steps') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.steps')) }}</span></a></li>
            <li><a href="{{ route('units') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.units')) }}</span></a></li>
            <li><a href="{{ route('notification_templates') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.notification_templates')) }}</span></a></li>
            <li><a href="{{ route('suspend_reasons') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.suspend_reasons')) }}</span></a></li>
        </ul>
    </li>
@endif
