
    <meta charset="UTF-8">
    <link rel="stylesheet" media="print" href="{{ elixir('css/print.min.css') }}">
    <link rel="stylesheet" href="{{ '/css/app1.min.css' }}">
    <link rel="stylesheet" href="{{ '/telerik/styles/kendo.common.min.css' }}">
    <link rel="stylesheet" href="{{ '/telerik/styles/kendo.bootstrap.min.css' }}">
    <link rel="stylesheet" href="{{ '/telerik/styles/kendo.default.mobile.min.css' }}">
    {{-- <script src="{{ '/bower_components/jquery/dist/jquery.min.js' }}"></script>
    <script src="{{ '/bower_components/bootstrap/dist/js/bootstrap.min.js' }}"></script>
    <script src="{{ '/bower_components/jquery-colorbox/jquery.colorbox-min.js' }}"></script> --}}
    <script src="{{ '/js/app1.min.js' }}"></script>
    <script src="{{ '/telerik/js/kendo.all.min.js' }}"></script>


<div  id="favoritesModal" style="position: absolute;top: 50%;left:50%;transform: translate(-50%,-50%)"
     tabindex="1" role="dialog"
     aria-labelledby="favoritesModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div style="top:100%" >
                    <div class="progress progress-striped " style="width:100%">
                        <div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="50" style="width: 100%;">

                        </div>

                    </div>  <span class="ladda-label" style="vertical-align: middle">{{ucfirst(trans('labels.inprogress'))}}</span>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    //$(document).ready(function () {
    var pb = $("#progressBar").kendoProgressBar({
        min: 0,
        max: 100,
        type: "value",
        value:0,
        animation: {
            duration: 0
        },
        complete: function(e){
            top.location.href='{{route('migrations')}}' ;
        },
        change:function(e){


        }
    }).data("kendoProgressBar");






    /*var valor= $(".selector").progressbar("option","disabled");
    alert(valor.value());
*/
</script>



