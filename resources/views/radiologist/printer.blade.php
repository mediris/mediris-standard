@extends('layouts.app')

@section('title',ucfirst(trans('titles.radiologist')))

@section('content')

    <div class="container-fluid radiologist printer">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel sombra">
                    <div class="panel-heading">
                        <div class="row flex">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <img src="{{ url('/images/logo_idaca.svg') }}" alt="">
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 flex-end">
                                <p class="institution_id text-right">Rif. {{ Session::get('institution')->institution_id }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="title">
                            <h1>{{ trans('labels.orders-to-dictate') }}</h1>
                        </div>

                        <div id="grid" class="k-grid k-widget" data-role="grid">
                            <div class="k-grid-header" style="padding-right: 10px;">

                                <div class="k-grid-header-wrap k-auto-scrollable">
                                    <table role="grid">
                                        <colgroup><col><col><col><col><col><col><col><col></colgroup>
                                        <thead role="rowgroup">
                                            <tr role="row">
                                                <th scope="col" role="columnheader" data-field="" rowspan="1" data-title=" " data-index="0" class="k-header" data-role="columnsorter"><a class="k-link" href="#"> </a></th>
                                                <th scope="col" role="columnheader" data-field="patientType" rowspan="1" data-title="{{ ucfirst(trans('labels.technician-patient-type')) }}" data-index="1" class="k-header k-with-icon k-filterable" data-role="columnsorter"><a class="k-link" href="#">{{ ucfirst(trans('labels.technician-patient-type')) }}</a></th>
                                                <th scope="col" role="columnheader" data-field="patientType" rowspan="1" data-title="{{ ucfirst(trans('labels.admission-date')) }}" data-index="1" class="k-header k-with-icon k-filterable" data-role="columnsorter"><a class="k-link" href="#">{{ ucfirst(trans('labels.admission-date')) }}</a></th>
                                                <th scope="col" role="columnheader" data-field="patientType" rowspan="1" data-title="{{ ucfirst(trans('labels.service-request-id')) }}" data-index="1" class="k-header k-with-icon k-filterable" data-role="columnsorter"><a class="k-link" href="#">{{ ucfirst(trans('labels.service-request-id')) }}</a></th>
                                                <th scope="col" role="columnheader" data-field="patientType" rowspan="1" data-title="{{ ucfirst(trans('labels.order-id')) }}" data-index="1" class="k-header k-with-icon k-filterable" data-role="columnsorter"><a class="k-link" href="#">{{ ucfirst(trans('labels.order-id')) }}</a></th>
                                                <th scope="col" role="columnheader" data-field="patientType" rowspan="1" data-title="{{ ucfirst(trans('labels.procedure')) }}" data-index="1" class="k-header k-with-icon k-filterable" data-role="columnsorter"><a class="k-link" href="#">{{ ucfirst(trans('labels.procedure')) }}</a></th>
                                                <th scope="col" role="columnheader" data-field="patientType" rowspan="1" data-title="{{ ucfirst(trans('labels.patient.first-name')) }}" data-index="1" class="k-header k-with-icon k-filterable" data-role="columnsorter"><a class="k-link" href="#">{{ ucfirst(trans('labels.patient.first-name')) }}</a></th>
                                                <th scope="col" role="columnheader" data-field="patientType" rowspan="1" data-title="{{ ucfirst(trans('labels.patient.last-name')) }}" data-index="1" class="k-header k-with-icon k-filterable" data-role="columnsorter"><a class="k-link" href="#">{{ ucfirst(trans('labels.patient.last-name')) }}</a></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                            <div class="k-grid-content k-auto-scrollable">
                                <table role="grid" style="height:auto">
                                    <colgroup>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                    </colgroup>

                                    <tbody role="rowgroup">
                                        @foreach($requestedProcedures as $requestedProcedure)
                                            <tr role="row">
                                                <td role="gridcell">
                                                    {{ html_entity_decode($requestedProcedure->service_request->patient_type->icon.';') }}
                                                </td>
                                                <td role="gridcell">
                                                    {{ $requestedProcedure->service_request->patient_type->description }}
                                                </td>
                                                <td role="gridcell">
                                                    {{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($requestedProcedure->service_request->issue_date)) : date('Y-m-d', strtotime($requestedProcedure->service_request->issue_date)) }}
                                                </td>
                                                <td role="gridcell">{{ $requestedProcedure->service_request->id }}</td>
                                                <td role="gridcell">{{ $requestedProcedure->id }}</td>
                                                <td role="gridcell">{{ $requestedProcedure->procedure->description }}</td>
                                                <td role="gridcell">{{ $requestedProcedure->patient->first_name }}</td>
                                                <td role="gridcell">{{ $requestedProcedure->patient->last_name }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

<script>
    window.document.close();
    window.focus();
    window.print();
</script>

