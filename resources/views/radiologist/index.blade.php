@extends('layouts.app')

@section('title', ucfirst(trans('labels.radiologist')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('labels.radiologist')),
                                    'elem_type' => '',
                                    'elem_name' => '',
                                    'form_id' => '',
                                    'route' => '',
                                    'fancybox' => '',
                                    'routeBack' => '',
                                    'clearFilters' => true
                                ])

    <!-- MODAL ASIGNAR ORDEN -->
    <div class="modal fade" id="assignOrderModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="cboxClose" data-dismiss="modal"><span>x</span></button>
                    <h4 class="modal-title">{{ ucfirst(trans('labels.to-assign')) }}</h4>
                </div>
                <form id="assignOrderForm" method="post" action="{{ route('radiologist.assign') }}">
                    <div class="modal-body">
                        <div>
                            @include('includes.assignRadiologist', [
                                'idname' => 'radiologist',
                                'data' => $radiologistsOrder,
                                'keys' => ['id', 'fullname']
                            ])
                        </div>
                        <input type="hidden" id="orderId" name="orderId" value="-1">
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="btn-add-edit-save" class="btn btn-form">{{ ucfirst(trans('labels.save')) }}</button>
                        @include('includes.select', [
                            'idname'    => 'user-change-sel',
                            'value'     => \Auth::user()->id,
                            'data'      => \App\User::all(),
                            'keys'      => ['id', 'username'],
                            'class'     => 'dropup in-footer'
                        ])
                        </div>
                </form>

            </div>
        </div>
    </div>
    <!-- MODAL ASIGNAR ORDEN -->

    <div class="container-fluid radiologist index">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <ul class="nav nav-tabs">
                    <li class="{{ $status == 1 || $status == null ? 'active' : '' }}">
                        <a data-toggle="tab" href="#orders-to-dictate">
                            <h1 class="tab-title">{{ trans('labels.orders-to-dictate') }}</h1>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#own-orders">
                            <h1 class="tab-title">{{ trans('labels.my-orders-dictate') }}</h1>
                        </a>
                    </li>
                    <li class="{{ $status == 2 ? 'active' : '' }}">
                        <a data-toggle="tab" href="#orders-to-approve">
                            <h1 class="tab-title">{{ trans('labels.orders-to-approve') }}</h1>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#own-orders-to-approve">
                            <h1 class="tab-title">{{ trans('labels.my-orders-to-approve') }}</h1>
                        </a>
                    </li>
                    <li class="{{ $status == 3 ? 'active' : '' }}">
                        <a data-toggle="tab" href="#addendum">
                            <h1 class="tab-title">{{ ucfirst(trans('labels.addendum')) }}</h1>
                        </a>
                    </li>
                    <li class="{{ $status == 4 ? 'active' : '' }}">
                        <a data-toggle="tab" href="#orders-dictated">
                            <h1 class="tab-title">{{ trans('labels.orders-dictated') }}</h1>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="orders-to-dictate"
                         class="tab-pane fade {{ $status == 1 || $status == null ? 'in active' : '' }}">
                        <div class="title row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <h1>{{ trans('labels.orders-to-dictate') }}</h1>
                            </div>
                        </div>

                        <?php

                        // Grid
                        $options = (object)array(
                            'url' => '/radiologist/3'
                        );
                        $kendo = new \App\CustomKendoGrid($options);

                        // Fields
                        $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                        $PatientTypeIconField->type('string');
                        $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                        $PatientTypeField->type('string');
                        $UrgentField = new \Kendo\Data\DataSourceSchemaModelField('urgent');
                        $UrgentField->type('string');
                        $AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
                        $AdmissionDateField->type('date');
                        $AdmissionHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
                        $AdmissionHourField->type('date');
                        $RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                        $RequestNumberField->type('string');
                        $OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                        $OrderNumberField->type('string');
                        $ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                        $ProcedureField->type('string');
                        $PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
                        $PatientFirstNameField->type('string');
                        $PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
                        $PatientLastNameField->type('string');
                        $PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                        $PatientIDField->type('string');
                        $BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
                        $BlockedStatusField->type('integer');
                        $BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
                        $BlockingUserId->type('integer');
                        $BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
                        $BlockingUserName->type('integer');
                        $BlockedStatusRadiologist = new \Kendo\Data\DataSourceSchemaModelField('blockedRadiologistStatus');
                        $BlockedStatusRadiologist->type('string');
                        $BlockingRadiologistId = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistID');
                        $BlockingRadiologistId->type('integer');
                        $BlockingRadiologistName = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistName');
                        $BlockingRadiologistName->type('string');

                        // Add Fields
                        $kendo->addFields(array(
                            $PatientTypeIconField,
                            $AdmissionDateField,
                            $AdmissionHourField,
                            $RequestNumberField,
                            $OrderNumberField,
                            $ProcedureField,
                            $PatientLastNameField,
                            $PatientFirstNameField,
                            $PatientIDField,
                            $BlockedStatusField,
                            $BlockingUserId,
                            $BlockingUserName,
                            $BlockedStatusRadiologist,
                            $BlockingRadiologistId,
                            $BlockingRadiologistName,
                            $UrgentField,

                        ));

                        // Create Schema
                        $kendo->createSchema(true, true);

                        // Create Data Source
                        $kendo->createDataSource();

                        // Create Grid
                        $kendo->createGrid(null, 'grid');

                        // Columns
                        $PatientTypeIcon = new \Kendo\UI\GridColumn();
                        $PatientTypeIcon->field('patientTypeIcon')
                            ->attributes(['class' => 'font-awesome-td'])
                            ->filterable(false)
                            ->encoded(false)
                            ->title(ucfirst(trans('labels.patient-type-icon')));

                        //$slash = "<span style='color:"+ row.patientTypeColor +"' ></span>";

                        $PatientTypeIcon
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientTypeIcon + '</span>';
                            }
                        "))
                            ->width(35);
                        $PatientType = new \Kendo\UI\GridColumn();
                        $PatientType->field('patientType')
                            ->title(ucfirst(trans('labels.technician-patient-type')))
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientType + '</span>';
                            }
						"))
                            ->width(120);

                        $Urgent = new \Kendo\UI\GridColumn();
                        $Urgent->title(ucfirst(trans('labels.urgent')))
                                ->template(new Kendo\JavaScriptFunction("function(row){
                                    if(row.urgent == 1)
                                    {
                                        return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                                    }
                                    else
                                    {
                                       return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                                    }
                                }"))
                                ->width(90);

                        $Modality = new \Kendo\UI\GridColumn();
                        $Modality->field('modality')
                            ->title(ucfirst(trans('labels.modality')))
                            ->width(135);

                        $AdmissionDate = new \Kendo\UI\GridColumn();
                        $AdmissionDate->field('serviceIssueDate')
                            ->format('{0: dd/MM/yyyy}')
                            ->title(ucfirst(trans('labels.admission-date')))
                            ->width(125);
                        $AdmissionHour = new \Kendo\UI\GridColumn();
                        $AdmissionHour->field('serviceIssueDate')
                            ->filterable(false)
                            ->format('{0: h:mm tt}')
                            ->title(ucfirst(trans('labels.admission-hour')))
                            ->width(97);
                        /*$RequestNumber = new \Kendo\UI\GridColumn();
                        $RequestNumber->field('serviceRequestID')
                                ->title(ucfirst(trans('labels.service-request-id')));*/
                        $OrderNumber = new \Kendo\UI\GridColumn();
                        $OrderNumber->field('orderID')
                            ->title(ucfirst(trans('labels.order-id')))
                            ->width(120);
                        $PatientIDField = new \Kendo\UI\GridColumn();
                        $PatientIDField->field('patientID')
                            ->title(ucfirst(trans('labels.patient-id')))
                            ->width(113);
                        $Procedure = new \Kendo\UI\GridColumn();
                        $Procedure->field('procedureDescription')
                            ->title(ucfirst(trans('labels.procedure')))
                            ->width(165);
                        $PatientFirstName = new \Kendo\UI\GridColumn();
                        $PatientFirstName->field('patientFirstName')
                            ->title(ucfirst(trans('labels.patient.first-name')))
                            ->width(120);
                        $PatientLastName = new \Kendo\UI\GridColumn();
                        $PatientLastName->field('patientLastName')
                            ->title(ucfirst(trans('labels.patient.last-name')))
                            ->width(120);

                        $actions = new \Kendo\UI\GridColumn();
                        $actions->title(trans('labels.actions'))
                        ->width(170);

                        $slash = "<span class='slash'>|</span>";

                        $canAssign = Session::get('roles')[0]->id == 1 || Session::get('roles')[0]->id == 4 || Session::get('roles')[0]->id == 99 ? 1 : 0;

                        $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');

                        $canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');

                        $lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedRadiologistStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingRadiologistName) + \"'><i class='fa \" + (row.blockedRadiologistStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');

                        $addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');

                        $editAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/edit') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "' target='_blank'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');

                        $assignAction = ($canAssign ? "+\"" . $slash . "\" + \"<div class='order-action suspend-order'><a onclick='assignOrder(\" + row.orderID + \");' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.to-assign') .
                        "'><i class='fa fa-check-square-o' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\"" : '');

                        $actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $lockAction . $addListAction . $editAction . $assignAction . ";
                            }"));

                        // Excel
                        $kendo->setExcel('titles.orders-to-dictate-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'), 'page-template1');

                        // PDF
                        $kendo->setPDF('titles.orders-to-dictate-list', 'radiologist/3/1', '
		                    function (e) {
		                        e.sender.hideColumn(6);
		                        e.promise.done(function() {
		                            e.sender.showColumn(6);
		                        });
		                    }', ' ' . date('d-M-Y'), 'page-template1');

                        // Filter
                        $kendo->setFilter();

                        // Pager
                        $kendo->setPager();

                        // Column Menu
                        $kendo->setColumnMenu();

                        $kendo->addcolumns(array(
                            $PatientTypeIcon,
                            $PatientType,
                            $Urgent,
                            $Modality,
                            $AdmissionDate,
                            $AdmissionHour,
                            // $RequestNumber,
                            $OrderNumber,
                            $PatientIDField,
                            $Procedure,
                            $PatientLastName,
                            $PatientFirstName,
                            $actions,
                        ));

                        $kendo->generate(true, true, 'row', 550);

                        ?>

                        {!! $kendo->render(); !!}
                    </div>

                    <div id="own-orders" class="tab-pane fade">
                        <div class="title row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <h1>{{ trans('labels.my-orders-dictate') }}</h1>
                            </div>
                        </div>

                        <?php

                        // Grid
                        $options = (object)array(
                            'url' => '/radiologist/3'
                        );
                        $kendo = new \App\CustomKendoGrid($options);

                        // Fields
                        $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                        $PatientTypeIconField->type('string');
                        $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                        $PatientTypeField->type('string');
                        $UrgentField = new \Kendo\Data\DataSourceSchemaModelField('urgent');
                        $UrgentField->type('string');
                        $AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
                        $AdmissionDateField->type('date');
                        $AdmissionHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
                        $AdmissionHourField->type('date');
                        $RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                        $RequestNumberField->type('string');
                        $OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                        $OrderNumberField->type('string');
                        $ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                        $ProcedureField->type('string');
                        $PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
                        $PatientFirstNameField->type('string');
                        $PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
                        $PatientLastNameField->type('string');
                        $PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                        $PatientIDField->type('string');
                        $BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
                        $BlockedStatusField->type('integer');
                        $BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
                        $BlockingUserId->type('integer');
                        $BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
                        $BlockingUserName->type('integer');
                        $BlockedStatusRadiologist = new \Kendo\Data\DataSourceSchemaModelField('blockedRadiologistStatus');
                        $BlockedStatusRadiologist->type('string');
                        $BlockingRadiologistId = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistID');
                        $BlockingRadiologistId->type('integer');
                        $BlockingRadiologistName = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistName');
                        $BlockingRadiologistName->type('string');

                        // Add Fields
                        $kendo->addFields(array(
                            $PatientTypeIconField,
                            $AdmissionDateField,
                            $AdmissionHourField,
                            $RequestNumberField,
                            $OrderNumberField,
                            $ProcedureField,
                            $PatientLastNameField,
                            $PatientFirstNameField,
                            $PatientIDField,
                            $BlockedStatusField,
                            $BlockingUserId,
                            $BlockingUserName,
                            $BlockedStatusRadiologist,
                            $BlockingRadiologistId,
                            $BlockingRadiologistName,
                            $UrgentField,


                        ));

                        // Create Schema
                        $kendo->createSchema(true, true);

                        // Create Data Source
                        $kendo->createDataSource(1, 10, array(
                            array('field' => 'blockedRadiologistStatus', 'value' => 1, 'operator' => 'eq'),
                            array('field' => 'blockingRadiologistID', 'value' => Auth::user()->id, 'operator' => 'eq')
                        ));

                        // Create Grid
                        $kendo->createGrid(null, 'grid1');

                        // Columns
                        $PatientTypeIcon = new \Kendo\UI\GridColumn();
                        $PatientTypeIcon->field('patientTypeIcon')
                            ->attributes(['class' => 'font-awesome-td'])
                            ->filterable(false)
                            ->encoded(false)
                            ->title(ucfirst(trans('labels.patient-type-icon')))
                            ->width(40);
                        $PatientTypeIcon
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientTypeIcon + '</span>';
                            }
                        "))
                            ->width(35);
                        $PatientType = new \Kendo\UI\GridColumn();
                        $PatientType->field('patientType')
                            ->title(ucfirst(trans('labels.technician-patient-type')))
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientType + '</span>';
                            }
						"))
                            ->width(120);
                        $Urgent = new \Kendo\UI\GridColumn();
                        $Urgent->title(ucfirst(trans('labels.urgent')))
                                ->template(new Kendo\JavaScriptFunction("function(row){
                                    if(row.urgent == 1)
                                    {
                                        return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                                    }
                                    else
                                    {
                                       return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                                    }
                                }"))
                                ->width(90);
                        $Modality = new \Kendo\UI\GridColumn();
                        $Modality->field('modality')
                            ->title(ucfirst(trans('labels.modality')))
                            ->width(140);
                        $AdmissionDate = new \Kendo\UI\GridColumn();
                        $AdmissionDate->field('serviceIssueDate')
                            ->format('{0: dd/MM/yyyy}')
                            ->title(ucfirst(trans('labels.admission-date')))
                            ->width(130);
                        $AdmissionHour = new \Kendo\UI\GridColumn();
                        $AdmissionHour->field('serviceIssueDate')
                            ->filterable(false)
                            ->format('{0: h:mm tt}')
                            ->title(ucfirst(trans('labels.admission-hour')))
                            ->width(100);
                        /*$RequestNumber = new \Kendo\UI\GridColumn();
                        $RequestNumber->field('serviceRequestID')
                                ->title(ucfirst(trans('labels.service-request-id')));*/
                        $OrderNumber = new \Kendo\UI\GridColumn();
                        $OrderNumber->field('orderID')
                            ->title(ucfirst(trans('labels.order-id')))
                            ->width(110);
                        $PatientIDField = new \Kendo\UI\GridColumn();
                        $PatientIDField->field('patientID')
                            ->title(ucfirst(trans('labels.patient-id')))
                            ->width(130);
                        $Procedure = new \Kendo\UI\GridColumn();
                        $Procedure->field('procedureDescription')
                            ->title(ucfirst(trans('labels.procedure')))
                            ->width(180);
                        $PatientFirstName = new \Kendo\UI\GridColumn();
                        $PatientFirstName->field('patientFirstName')
                            ->title(ucfirst(trans('labels.patient.first-name')))
                            ->width(120);
                        $PatientLastName = new \Kendo\UI\GridColumn();
                        $PatientLastName->field('patientLastName')
                            ->title(ucfirst(trans('labels.patient.last-name')))
                            ->width(120);

                        $actions = new \Kendo\UI\GridColumn();
                        $actions->title(trans('labels.actions'))
                                ->width(130);

                        $slash = "<span class='slash'>|</span>";

                        $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');

                        $canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');

                        $lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedRadiologistStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingRadiologistName) + \"'><i class='fa \" + (row.blockedRadiologistStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');

                        $addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' href='javascript:;' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . "data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');

                        $editAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/edit') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "' target='_blank'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');


                        $actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $lockAction . $addListAction . $editAction . ";
                            }"));

                        // Excel
                        $kendo->setExcel('titles.my-orders-to-dictate-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

                        // PDF
                        $kendo->setPDF('titles.my-orders-to-dictate-list', 'radiologist/3/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template2');

                        // Filter
                        $kendo->setFilter();

                        // Pager
                        $kendo->setPager();

                        // Column Menu
                        $kendo->setColumnMenu();

                        $kendo->addcolumns(array(
                            $PatientTypeIcon,
                            $PatientType,
                            $Urgent,
                            $Modality,
                            $AdmissionDate,
                            $AdmissionHour,
                            // $RequestNumber,
                            $OrderNumber,
                            $PatientIDField,
                            $Procedure,
                            $PatientLastName,
                            $PatientFirstName,
                            $actions,
                        ));

                        $kendo->generate(true, true, 'row', 550);

                        ?>

                        {!! $kendo->render() !!}

                    </div>

                    <div id="orders-to-approve" class="tab-pane fade {{ $status == 2 ? 'in active' : '' }}">
                        <div class="title row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <h1>{{ trans('labels.orders-to-approve') }}</h1>
                            </div>
                        </div>

                        <?php

                        // Grid
                        $options = (object)array(
                            'url' => '/radiologist/5'
                        );
                        $kendo = new \App\CustomKendoGrid($options);

                        // Fields
                        $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                        $PatientTypeIconField->type('string');
                        $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                        $PatientTypeField->type('string');
                        $UrgentField = new \Kendo\Data\DataSourceSchemaModelField('urgent');
                        $UrgentField->type('string');
                        $TranscriptionDateField = new \Kendo\Data\DataSourceSchemaModelField('transcriptionDate');
                        $TranscriptionDateField->type('date');
                        $RadiologistUserName = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserName');
                        $RadiologistUserName->type('string');
                        /*$TranscriptionHourField = new \Kendo\Data\DataSourceSchemaModelField('transcriptionDate');
                        $TranscriptionHourField->type('date');*/
                        $RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                        $RequestNumberField->type('string');
                        $OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                        $OrderNumberField->type('string');
                        $ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                        $ProcedureField->type('string');
                        $PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
                        $PatientFirstNameField->type('string');
                        $PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
                        $PatientLastNameField->type('string');
                        $PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                        $PatientIDField->type('string');
                        $BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
                        $BlockedStatusField->type('integer');
                        $BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
                        $BlockingUserId->type('integer');
                        $BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
                        $BlockingUserName->type('integer');
                        $BlockedStatusRadiologist = new \Kendo\Data\DataSourceSchemaModelField('blockedRadiologistStatus');
                        $BlockedStatusRadiologist->type('string');
                        $BlockingRadiologistId = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistID');
                        $BlockingRadiologistId->type('integer');
                        $BlockingRadiologistName = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistName');
                        $BlockingRadiologistName->type('string');

                        // Add Fields
                        $kendo->addFields(array(
                            $PatientTypeIconField,
                            $TranscriptionDateField,
                            $RadiologistUserName,
                            //$TranscriptionHourField,
                            $RequestNumberField,
                            $OrderNumberField,
                            $ProcedureField,
                            $PatientFirstNameField,
                            $PatientLastNameField,
                            $PatientIDField,
                            $BlockedStatusField,
                            $BlockingUserId,
                            $BlockingUserName,
                            $BlockedStatusRadiologist,
                            $BlockingRadiologistId,
                            $BlockingRadiologistName,
                            $UrgentField,

                        ));

                        // Create Schema
                        $kendo->createSchema(true, true);

                        // Create Data Source
                        $kendo->createDataSource();

                        // Create Grid
                        $kendo->createGrid(null, 'grid2');

                        // Columns
                        $PatientTypeIcon = new \Kendo\UI\GridColumn();
                        $PatientTypeIcon->field('patientTypeIcon')
                            ->attributes(['class' => 'font-awesome-td'])
                            ->filterable(false)
                            ->encoded(false)
                            ->title(ucfirst(trans('labels.patient-type-icon')))
                            ->width(35);
                        $PatientTypeIcon
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientTypeIcon + '</span>';
                            }
                        "));
                        $PatientType = new \Kendo\UI\GridColumn();
                        $PatientType->field('patientType')
                            ->title(ucfirst(trans('labels.technician-patient-type')))
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientType + '</span>';
                            }
						"))
                            ->width(85);
                        $Urgent = new \Kendo\UI\GridColumn();
                        $Urgent->title(ucfirst(trans('labels.urgent')))
                                ->template(new Kendo\JavaScriptFunction("function(row){
                                    if(row.urgent == 1)
                                    {
                                        return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                                    }
                                    else
                                    {
                                       return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                                    }
                                }"))
                                ->width(90);
                        $TranscriptionDate = new \Kendo\UI\GridColumn();
                        $TranscriptionDate->field('transcriptionDate')
                            ->format('{0: dd/MM/yyyy}')
                            ->title(ucfirst(trans('labels.transcription-date')))
                            ->width(110);
                        $RadiologistUserName = new \Kendo\UI\GridColumn();
                        $RadiologistUserName->field('radiologistUserName')
                            ->title(ucfirst(trans('labels.dictated-by')))
                            ->width(80);
                        /*$TranscriptionHour = new \Kendo\UI\GridColumn();
                        $TranscriptionHour->field('transcriptionDate')
                                ->filterable(false)
                                ->format('{0: h:mm tt}')
                                ->title(ucfirst(trans('labels.transcription-hour')));*/
                        /*$RequestNumber = new \Kendo\UI\GridColumn();
                        $RequestNumber->field('serviceRequestID')
                                ->title(ucfirst(trans('labels.service-request-id')));*/
                        $OrderNumber = new \Kendo\UI\GridColumn();
                        $OrderNumber->field('orderID')
                            ->title(ucfirst(trans('labels.order-id')))
                            ->width(70);
                        $PatientIDField = new \Kendo\UI\GridColumn();
                        $PatientIDField->field('patientID')
                            ->title(ucfirst(trans('labels.patient-id')))
                            ->width(80);
                        $Procedure = new \Kendo\UI\GridColumn();
                        $Procedure->field('procedureDescription')
                            ->title(ucfirst(trans('labels.procedure')))
                            ->width(110);
                        $PatientFirstName = new \Kendo\UI\GridColumn();
                        $PatientFirstName->field('patientFirstName')
                            ->title(ucfirst(trans('labels.patient.first-name')))
                            ->width(80);
                        $PatientLastName = new \Kendo\UI\GridColumn();
                        $PatientLastName->field('patientLastName')
                            ->title(ucfirst(trans('labels.patient.last-name')))
                            ->width(80);

                        $actions = new \Kendo\UI\GridColumn();
                        $actions->title(trans('labels.actions'))
                                ->width(90);

                        $slash = "<span class='slash'>|</span>";

                        $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');

                        $canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');

                        $canApprove = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'approve');

                        $lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedRadiologistStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingRadiologistName) + \"'><i class='fa \" + (row.blockedRadiologistStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');

                        $addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');

                        $approveAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/approve') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "' target='_blank'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');


                        $actions->template(new Kendo\JavaScriptFunction("
						   function (row)
						   {
						      return  " . $lockAction . $addListAction . $approveAction . ";
						   }"
                        ));

                        // Excel
                        $kendo->setExcel('titles.orders-to-approve-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

                        // PDF
                        $kendo->setPDF('titles.orders-to-approve-list', 'radiologist/5/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template3');

                        // Filter
                        $kendo->setFilter();

                        // Pager
                        $kendo->setPager();

                        // Column Menu
                        $kendo->setColumnMenu();

                        $kendo->addcolumns(array(
                            $PatientTypeIcon,
                            $PatientType,
                            $Urgent,
                            $TranscriptionDate,
                            $RadiologistUserName,
                            //$TranscriptionHour,
                            // $RequestNumber,
                            $OrderNumber,
                            $PatientIDField,
                            $Procedure,
                            $PatientLastName,
                            $PatientFirstName,
                            $actions,
                        ));

                        $kendo->generate(true, true, 'row', 550);

                        ?>

                        {!! $kendo->render() !!}

                    </div>

                    <div id="own-orders-to-approve" class="tab-pane fade">
                        <div class="title row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <h1>{{ trans('labels.my-orders-to-approve') }}</h1>
                            </div>
                        </div>

                        <?php

                        // Grid
                        $options = (object)array(
                            'url' => '/radiologist/5'
                        );
                        $kendo = new \App\CustomKendoGrid($options);

                        // Fields
                        $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                        $PatientTypeIconField->type('string');
                        $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                        $PatientTypeField->type('string');
                        $UrgentField = new \Kendo\Data\DataSourceSchemaModelField('urgent');
                        $UrgentField->type('string');
                        $AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
                        $AdmissionDateField->type('date');
                        $AdmissionHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
                        $AdmissionHourField->type('date');
                        $RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                        $RequestNumberField->type('string');
                        $OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                        $OrderNumberField->type('string');
                        $ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                        $ProcedureField->type('string');
                        $PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
                        $PatientFirstNameField->type('string');
                        $PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
                        $PatientLastNameField->type('string');
                        $PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                        $PatientIDField->type('string');
                        $BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
                        $BlockedStatusField->type('integer');
                        $BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
                        $BlockingUserId->type('integer');
                        $BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
                        $BlockingUserName->type('integer');
                        $BlockedStatusRadiologist = new \Kendo\Data\DataSourceSchemaModelField('blockedRadiologistStatus');
                        $BlockedStatusRadiologist->type('string');
                        $BlockingRadiologistId = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistID');
                        $BlockingRadiologistId->type('integer');
                        $BlockingRadiologistName = new \Kendo\Data\DataSourceSchemaModelField('blockingRadiologistName');
                        $BlockingRadiologistName->type('string');

                        // Add Fields
                        $kendo->addFields(array(
                            $PatientTypeIconField,
                            $AdmissionDateField,
                            $AdmissionHourField,
                            $RequestNumberField,
                            $OrderNumberField,
                            $ProcedureField,
                            $PatientFirstNameField,
                            $PatientLastNameField,
                            $PatientIDField,
                            $BlockedStatusField,
                            $BlockingUserId,
                            $BlockingUserName,
                            $BlockedStatusRadiologist,
                            $BlockingRadiologistId,
                            $BlockingRadiologistName,
                            $UrgentField,

                        ));

                        // Create Schema
                        $kendo->createSchema(true, true);

                        // Create Data Source
                        $kendo->createDataSource(1, 10, array(
                            array('field' => 'blockedRadiologistStatus', 'value' => 1, 'operator' => 'eq'),
                            array('field' => 'blockingRadiologistID', 'value' => Auth::user()->id, 'operator' => 'eq'),
                        ));

                        // Create Grid
                        $kendo->createGrid(null, 'grid5');

                        // Columns
                        $PatientTypeIcon = new \Kendo\UI\GridColumn();
                        $PatientTypeIcon->field('patientTypeIcon')
                            ->attributes(['class' => 'font-awesome-td'])
                            ->filterable(false)
                            ->encoded(false)
                            ->title(ucfirst(trans('labels.patient-type-icon')))
                            ->width(50);
                        $PatientTypeIcon
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientTypeIcon + '</span>';
                            }
                        "))
                             ->width(40);
                        $PatientType = new \Kendo\UI\GridColumn();
                        $PatientType->field('patientType')
                            ->title(ucfirst(trans('labels.technician-patient-type')))
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientType + '</span>';
                            }
						"))
                            ->width(90);
                        $Urgent = new \Kendo\UI\GridColumn();
                        $Urgent->title(ucfirst(trans('labels.urgent')))
                                ->template(new Kendo\JavaScriptFunction("function(row){
                                    if(row.urgent == 1)
                                    {
                                        return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                                    }
                                    else
                                    {
                                       return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                                    }
                                }"))
                                ->width(90);
                        $AdmissionDate = new \Kendo\UI\GridColumn();
                        $AdmissionDate->field('serviceIssueDate')
                            ->format('{0: dd/MM/yyyy}')
                            ->title(ucfirst(trans('labels.admission-date')))
                            ->width(100);
                        $AdmissionHour = new \Kendo\UI\GridColumn();
                        $AdmissionHour->field('serviceIssueDate')
                            ->filterable(false)
                            ->format('{0: h:mm tt}')
                            ->title(ucfirst(trans('labels.admission-hour')))
                            ->width(100);
                        /*$RequestNumber = new \Kendo\UI\GridColumn();
                        $RequestNumber->field('serviceRequestID')
                                ->title(ucfirst(trans('labels.service-request-id')));*/
                        $OrderNumber = new \Kendo\UI\GridColumn();
                        $OrderNumber->field('orderID')
                            ->title(ucfirst(trans('labels.order-id')))
                            ->width(100);
                        $PatientIDField = new \Kendo\UI\GridColumn();
                        $PatientIDField->field('patientID')
                            ->title(ucfirst(trans('labels.patient-id')))
                            ->width(100);
                        $Procedure = new \Kendo\UI\GridColumn();
                        $Procedure->field('procedureDescription')
                            ->title(ucfirst(trans('labels.procedure')))
                            ->width(120);
                        $PatientFirstName = new \Kendo\UI\GridColumn();
                        $PatientFirstName->field('patientFirstName')
                            ->title(ucfirst(trans('labels.patient.first-name')))
                            ->width(100);
                        $PatientLastName = new \Kendo\UI\GridColumn();
                        $PatientLastName->field('patientLastName')
                            ->title(ucfirst(trans('labels.patient.last-name')))
                            ->width(100);
                        $actions = new \Kendo\UI\GridColumn();
                        $actions->title(trans('labels.actions'))
                                ->width(100);

                        $slash = "<span class='slash'>|</span>";

                        $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');

                        $canLock = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'lock');

                        $lockAction = ($canLock ? "\"<div class='order-action block-order'><a class='change-order-status-radiologist' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . " href='javascript:;' data-placement='bottom' title='\" + (row.blockedRadiologistStatus == 0 ? '" . trans('labels.block-order') . "' : '" . trans('labels.order-blocked-by') . " ' + row.blockingRadiologistName) + \"'><i class='fa \" + (row.blockedRadiologistStatus == 0 ? \"fa-unlock\" : \"fa-lock\") + \"' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\" + " : '');

                        $addListAction = ($canEdit ? "\"" . $slash . "\" + \"<a class='addToListRadiologist' href='javascript:;' data-toggle='tooltip' attr-id=" . "\" + row.orderID + \"" . "data-placement='bottom' title='" . trans('labels.add-list') . "'><i class='fa fa-plus' aria-hidden='true'></i></a>\" + " : '');

                        $editAction = ($canEdit ? "\"" . $slash . "\" + \"<a href='" . url('radiologist/approve') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "' target='_blank'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');


                        $actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $lockAction . $addListAction . $editAction . ";
                            }"));

                        // Excel
                        $kendo->setExcel('titles.my-orders-to-approve-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

                        // PDF
                        $kendo->setPDF('titles.my-orders-to-approve-list', 'radiologist/5/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template4');

                        // Filter
                        $kendo->setFilter();

                        // Pager
                        $kendo->setPager();

                        // Column Menu
                        $kendo->setColumnMenu();

                        $kendo->addcolumns(array(
                            $PatientTypeIcon,
                            $PatientType,
                            $Urgent,
                            $AdmissionDate,
                            $AdmissionHour,
                            // $RequestNumber,
                            $OrderNumber,
                            $PatientIDField,
                            $Procedure,
                            $PatientLastName,
                            $PatientFirstName,
                            $actions,
                        ));

                        $kendo->generate(true, true, 'row', 550);

                        ?>

                        {!! $kendo->render() !!}

                    </div>

                    <div id="addendum" class="tab-pane fade {{ $status == 3 ? 'in active' : '' }}">
                        <div class="title row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <h1>{{ trans('labels.addendum') }}</h1>
                            </div>
                        </div>

                        <?php

                        // Grid
                        $options = (object)array(
                            'url' => '/radiologist/6'
                        );
                        $kendo = new \App\CustomKendoGrid($options);

                        // Fields
                        $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                        $PatientTypeIconField->type('string');
                        $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                        $PatientTypeField->type('string');
                        $UrgentField = new \Kendo\Data\DataSourceSchemaModelField('urgent');
                        $UrgentField->type('string');
                        $ApprovalDateField = new \Kendo\Data\DataSourceSchemaModelField('approvalDate');
                        $ApprovalDateField->type('date');
                        $ApprovalHourField = new \Kendo\Data\DataSourceSchemaModelField('approvalDate');
                        $ApprovalHourField->type('date');
                        $RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                        $RequestNumberField->type('string');
                        $OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                        $OrderNumberField->type('string');
                        $ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                        $ProcedureField->type('string');
                        $PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
                        $PatientFirstNameField->type('string');
                        $PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
                        $PatientLastNameField->type('string');
                        $PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                        $PatientIDField->type('string');
                        $BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
                        $BlockedStatusField->type('integer');
                        $BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
                        $BlockingUserId->type('integer');
                        $BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
                        $BlockingUserName->type('string');

                        // Add Fields
                        $kendo->addFields(array(
                            $PatientTypeIconField,
                            $ApprovalDateField,
                            $ApprovalHourField,
                            $RequestNumberField,
                            $OrderNumberField,
                            $ProcedureField,
                            $PatientFirstNameField,
                            $PatientLastNameField,
                            $PatientIDField,
                            $BlockedStatusField,
                            $BlockingUserId,
                            $BlockingUserName,
                            $UrgentField,
                        ));

                        // Create Schema
                        $kendo->createSchema(true, true);

                        // Create Data Source
                        $kendo->createDataSource();

                        // Create Grid
                        $kendo->createGrid(null, 'grid3');

                        // Columns
                        $PatientTypeIcon = new \Kendo\UI\GridColumn();
                        $PatientTypeIcon->field('patientTypeIcon')
                            ->attributes(['class' => 'font-awesome-td'])
                            ->filterable(false)
                            ->encoded(false)
                            ->title(ucfirst(trans('labels.patient-type-icon')));
                        $PatientTypeIcon
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientTypeIcon + '</span>';
                            }
                        "))
                            ->width(35);
                        $PatientType = new \Kendo\UI\GridColumn();
                        $PatientType->field('patientType')
                            ->title(ucfirst(trans('labels.technician-patient-type')))
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientType + '</span>';
                            }
						"))
                            ->width(80);
                        $Urgent = new \Kendo\UI\GridColumn();
                        $Urgent->title(ucfirst(trans('labels.urgent')))
                                ->template(new Kendo\JavaScriptFunction("function(row){
                                    if(row.urgent == 1)
                                    {
                                        return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                                    }
                                    else
                                    {
                                       return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                                    }
                                }"))
                                ->width(90);
                        $ApprovalDate = new \Kendo\UI\GridColumn();
                        $ApprovalDate->field('approvalDate')
                            ->format('{0: dd/MM/yyyy}')
                            ->title(ucfirst(trans('labels.approval-date')))
                            ->width(95);
                        $ApprovalHour = new \Kendo\UI\GridColumn();
                        $ApprovalHour->field('approvalDate')
                            ->filterable(false)
                            ->format('{0: h:mm tt}')
                            ->title(ucfirst(trans('labels.approval-hour')))
                            ->width(80);
                        /*$RequestNumber = new \Kendo\UI\GridColumn();
                        $RequestNumber->field('serviceRequestID')
                                ->title(ucfirst(trans('labels.service-request-id')));*/
                        $OrderNumber = new \Kendo\UI\GridColumn();
                        $OrderNumber->field('orderID')
                            ->title(ucfirst(trans('labels.order-id')))
                            ->width(80);
                        $PatientIDField = new \Kendo\UI\GridColumn();
                        $PatientIDField->field('patientID')
                            ->title(ucfirst(trans('labels.patient-id')))
                            ->width(90);
                        $Procedure = new \Kendo\UI\GridColumn();
                        $Procedure->field('procedureDescription')
                            ->title(ucfirst(trans('labels.procedure')))
                            ->width(120);
                        $PatientFirstName = new \Kendo\UI\GridColumn();
                        $PatientFirstName->field('patientFirstName')
                            ->title(ucfirst(trans('labels.patient.first-name')))
                            ->width(100);
                        $PatientLastName = new \Kendo\UI\GridColumn();
                        $PatientLastName->field('patientLastName')
                            ->title(ucfirst(trans('labels.patient.last-name')))
                            ->width(100);
                        $actions = new \Kendo\UI\GridColumn();
                        $actions->title(trans('labels.actions'))
                                ->width(60);

                        $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'edit');

                        $editAction = ($canEdit ? "\"<a href='" . url('radiologist/addendum') . "/\" + row.orderID + \"" . "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') . "' target='_blank'><i class='fa fa-eye' aria-hidden='true'></i></a>\"" : '');

                        $actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $editAction . ";
                            }"));

                        // Excel
                        $kendo->setExcel('titles.addendums-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

                        // PDF
                        $kendo->setPDF('titles.addendums-list', 'radiologist/6/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template5');

                        // Filter
                        $kendo->setFilter();

                        // Pager
                        $kendo->setPager();

                        // Column Menu
                        $kendo->setColumnMenu();

                        $kendo->addcolumns(array(
                            $PatientTypeIcon,
                            $PatientType,
                            $Urgent,
                            $ApprovalDate,
                            $ApprovalHour,
                            // $RequestNumber,
                            $OrderNumber,
                            $PatientIDField,
                            $Procedure,
                            $PatientLastName,
                            $PatientFirstName,
                            $actions,
                        ));

                        // // Auto Bind (Not Data Start)
                        // $kendo->autoBind(false);

                        $kendo->generate(true, true, 'row', 550);

                        ?>

                        {!! $kendo->render() !!}

                    </div>

<!--                     <script>
                        $("#grid3").kendoGrid({
                            autobind: false
                        });
                    </script> -->

                    <div id="orders-dictated" class="tab-pane fade {{ $status == 4 ? 'in active' : '' }}">
                        <div class="title row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <h1>{{ trans('labels.orders-dictated') }}</h1>
                            </div>
                        </div>

                        <?php

                        // Grid
                        $options = (object)array(
                            'url' => '/radiologist/4'
                        );
                        $kendo = new \App\CustomKendoGrid($options);

                        // Fields
                        $PatientTypeIconField = new \Kendo\Data\DataSourceSchemaModelField('patientTypeIcon');
                        $PatientTypeIconField->type('string');
                        $PatientTypeField = new \Kendo\Data\DataSourceSchemaModelField('patientType');
                        $PatientTypeField->type('string');
                        $UrgentField = new \Kendo\Data\DataSourceSchemaModelField('urgent');
                        $UrgentField->type('string');
                        $DictationDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceDictationDate');
                        $DictationDateField->type('date');
                        $DictationHourField = new \Kendo\Data\DataSourceSchemaModelField('serviceDictationDate');
                        $DictationHourField->type('date');
                        // $AdmissionDateField = new \Kendo\Data\DataSourceSchemaModelField('serviceIssueDate');
                        // $AdmissionDateField->type('date');
                        $RequestNumberField = new \Kendo\Data\DataSourceSchemaModelField('serviceRequestID');
                        $RequestNumberField->type('string');
                        $OrderNumberField = new \Kendo\Data\DataSourceSchemaModelField('orderID');
                        $OrderNumberField->type('string');
                        $ProcedureField = new \Kendo\Data\DataSourceSchemaModelField('procedureDescription');
                        $ProcedureField->type('string');
                        $PatientFirstNameField = new \Kendo\Data\DataSourceSchemaModelField('patientFirstName');
                        $PatientFirstNameField->type('string');
                        $PatientLastNameField = new \Kendo\Data\DataSourceSchemaModelField('patientLastName');
                        $PatientLastNameField->type('string');
                        $PatientIDField = new \Kendo\Data\DataSourceSchemaModelField('patientID');
                        $PatientIDField->type('string');
                        $BlockedStatusField = new \Kendo\Data\DataSourceSchemaModelField('blockedStatus');
                        $BlockedStatusField->type('integer');
                        $BlockingUserId = new \Kendo\Data\DataSourceSchemaModelField('blockingUserId');
                        $BlockingUserId->type('integer');
                        $BlockingUserName = new \Kendo\Data\DataSourceSchemaModelField('blockingUserName');
                        $BlockingUserName->type('integer');
                        $RadiologistUserId = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserID');
                        $RadiologistUserId->type('integer');
                        $RadiologistUserName = new \Kendo\Data\DataSourceSchemaModelField('radiologistUserName');
                        $RadiologistUserName->type('string');

                        // Add Fields
                        $kendo->addFields(array(
                            $PatientTypeIconField,
                            // $AdmissionDateField,
                            $UrgentField,
                            $DictationDateField,
                            $DictationHourField,
                            $RequestNumberField,
                            $OrderNumberField,
                            $ProcedureField,
                            $PatientLastNameField,
                            $PatientFirstNameField,
                            $PatientIDField,
                            $BlockedStatusField,
                            $BlockingUserId,
                            $BlockingUserName,
                            $RadiologistUserId,
                            $RadiologistUserName,
                        ));

                        // Create Schema
                        $kendo->createSchema(true, true);

                        // Create Data Source
                        $kendo->createDataSource(1, 10, array(
                            array('field' => 'approveID', 'value' => Auth::user()->id, 'operator' => 'eq'),
                            //array('field' => 'radiologistUserID', 'value' => Auth::user()->id, 'operator' => 'eq'),
                            //array('field' => 'blockedStatus', 'value' => 0, 'operator' => 'eq'),
                        ));

                        // Create Grid
                        $kendo->createGrid(null, 'grid4');

                        // Columns
                        $PatientTypeIcon = new \Kendo\UI\GridColumn();
                        $PatientTypeIcon->field('patientTypeIcon')
                            ->attributes(['class' => 'font-awesome-td'])
                            ->filterable(false)
                            ->encoded(false)
                            ->title(ucfirst(trans('labels.patient-type-icon')));
                        $PatientTypeIcon
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientTypeIcon + '</span>';
                            }
                        "))
                            ->width(35);
                        $PatientType = new \Kendo\UI\GridColumn();
                        $PatientType->field('patientType')
                            ->title(ucfirst(trans('labels.technician-patient-type')))
                            ->template(new Kendo\JavaScriptFunction("
                            function(row) {
                                return '<span style=color:' + row.patientTypeColor + '>' + row.patientType + '</span>';
                            }
						"))
                            ->width(60);
                        // $AdmissionDate = new \Kendo\UI\GridColumn();
                        // $AdmissionDate->field('serviceIssueDate')
                        // 		->format('{0: dd/MM/yyyy}')
                        // 		->title(ucfirst(trans('labels.admission-date')));
                        $Urgent = new \Kendo\UI\GridColumn();
                        $Urgent->title(ucfirst(trans('labels.urgent')))
                                ->template(new Kendo\JavaScriptFunction("function(row){
                                    if(row.urgent == 1)
                                    {
                                        return '<i class=\"fa fa-exclamation-triangle red-danger\" aria-hidden=\"true\"></i>';
                                    }
                                    else
                                    {
                                       return '<i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>'
                                    }
                                }"))
                                ->width(90);
                        $DictationDate = new \Kendo\UI\GridColumn();
                        $DictationDate->field('serviceDictationDate')
                            ->format('{0: dd/MM/yyyy}')
                            ->title(ucfirst(trans('labels.dictation-date')))
                            ->width(60);
                        $RadiologistUserName = new \Kendo\UI\GridColumn();
                        $RadiologistUserName->field('radiologistUserName')
                            ->title(ucfirst(trans('labels.dictated-by')))
                            ->width(60);
                        /*$DictationHour = new \Kendo\UI\GridColumn();
                        $DictationHour->field('serviceDictationDate')
                                ->filterable(false)
                                ->format('{0: h:mm tt}')
                                ->title(ucfirst(trans('labels.dictation-hour')));*/
                        /*$RequestNumber = new \Kendo\UI\GridColumn();
                        $RequestNumber->field('serviceRequestID')
                                ->title(ucfirst(trans('labels.service-request-id')));*/
                        $OrderNumber = new \Kendo\UI\GridColumn();
                        $OrderNumber->field('orderID')
                            ->title(ucfirst(trans('labels.order-id')))
                            ->width(50);
                        $PatientIDField = new \Kendo\UI\GridColumn();
                        $PatientIDField->field('patientID')
                            ->title(ucfirst(trans('labels.patient-id')))
                            ->width(60);
                        $Procedure = new \Kendo\UI\GridColumn();
                        $Procedure->field('procedureDescription')
                            ->title(ucfirst(trans('labels.procedure')))
                            ->width(110);
                        $PatientFirstName = new \Kendo\UI\GridColumn();
                        $PatientFirstName->field('patientFirstName')
                            ->title(ucfirst(trans('labels.patient.first-name')))
                            ->width(60);
                        $PatientLastName = new \Kendo\UI\GridColumn();
                        $PatientLastName->field('patientLastName')
                            ->title(ucfirst(trans('labels.patient.last-name')))
                            ->width(60);
                        $actions = new \Kendo\UI\GridColumn();
                        $actions->title(trans('labels.actions'))
                                ->width(60);

                        $slash = "<span class='slash'>|</span>";

                        $canRevert = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'radiologist', 'revert');

                        $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'search', 'edit');

                        $revertAction = ($canRevert ? "\"" . '' . "\" + \"<div><a href='javascript:;' class='revert-order' attr-id=\" + row.orderID + \" data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.revert-order') . "'><i class='fa fa-hand-paper-o' aria-hidden='true'></i></a><div class='loader'><img src='/images/loader.gif'></div></div>\"" : '');

                        $orderDetail = ($canEdit ? "\"<div class='order-action view-order-detail'><a href='" . url('search/edit') .
                            "/\" + row.orderID + \"" .
                            "' data-toggle='tooltip' data-placement='bottom' title='" . trans('labels.view-order-details') .
                            "' target='_blank'><i class='fa fa-eye' aria-hidden='true'></i></a></div>\"" : '');

                        $actions->template(new Kendo\JavaScriptFunction("
                            function (row)
                            {
                                return  " . $orderDetail . ";
                                // return  " . $revertAction . ";
                            }"));

                        // Excel
                        $kendo->setExcel('titles.dictated-orders-list', '
		                    function(e) {
				                if (!exportFlag) {
						            e.sender.hideColumn(0);
						            e.sender.hideColumn(8);
						            e.preventDefault();
						            exportFlag = true;
						            setTimeout(function () {
						              e.sender.saveAsExcel();
						            });
						          } else {
						            e.sender.showColumn(0);
						            e.sender.showColumn(8);
						            exportFlag = false;
						          }
						    }', '.xlsx', ' ' . date('d-M-Y'));

                        // PDF
                        $kendo->setPDF('titles.dictated-orders-list', 'radiologist/4/1', '
		                    function (e) {
								e.sender.hideColumn(0);
			                    e.sender.hideColumn(8);
								e.promise.done(function() {
									e.sender.showColumn(0);
			                        e.sender.showColumn(8);
								});
							}', ' ' . date('d-M-Y'), 'page-template6');

                        // Filter
                        $kendo->setFilter();

                        // Pager
                        $kendo->setPager();

                        // Column Menu
                        $kendo->setColumnMenu();

                        $kendo->addcolumns(array(
                            $PatientTypeIcon,
                            $PatientType,
                            $Urgent,
                            $DictationDate,
                            $RadiologistUserName,
                            //$DictationHour,
                            // $AdmissionDate,
                            // $RequestNumber,
                            $OrderNumber,
                            $PatientIDField,
                            $Procedure,
                            $PatientLastName,
                            $PatientFirstName,
                            $actions,
                        ));

                        $kendo->generate(true, true, 'row', 550);

                        ?>

                        {!! $kendo->render() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            // Refresh Kendo Grid each 30s
            kendoGridAutoRefresh('#grid', 20);
            kendoGridAutoRefresh('#grid1', 20);
            kendoGridAutoRefresh('#grid2', 20);
            kendoGridAutoRefresh('#grid3', 20);
            kendoGridAutoRefresh('#grid4', 20);
            kendoGridAutoRefresh('#grid5', 20);
            kendoGridAutoRefresh();

        });
    </script>

    <script type="x/kendo-template" id="page-template1">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
                    #
                </div>
                {{ trans('titles.orders-to-dictate-list') }}
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>

    <script type="x/kendo-template" id="page-template2">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
                    #
                </div>
                {{ trans('titles.my-orders-to-dictate-list') }}
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>

    <script type="x/kendo-template" id="page-template3">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
                    #
                </div>
                {{ trans('titles.orders-to-approve-list') }}
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>

    <script type="x/kendo-template" id="page-template4">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
                    #
                </div>
                {{ trans('titles.my-orders-to-approve-list') }}
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>

    <script type="x/kendo-template" id="page-template5">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
                    #
                </div>
                {{ trans('titles.addendums-list') }}
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>

    <script type="x/kendo-template" id="page-template6">
        <div class="page-template">
            <div class="header">
                <div style="float: right">{{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages
                    #
                </div>
                {{ trans('titles.dictated-orders-list') }}
            </div>
            <div class="watermark">{{ Session::get('institution')->name }}</div>
            <div class="footer">
                {{ trans('labels.page') }} #: pageNum # {{ trans('labels.of') }} #: totalPages #
            </div>
        </div>
    </script>

    <style type="text/css">
        /* Page Template for the exported PDF */
        .page-template {
            font-family: "Open Sans", "Arial", sans-serif;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }

        .page-template .header {
            position: absolute;
            top: 30px;
            left: 30px;
            right: 30px;
            border-bottom: 1px solid #888;
            color: #888;
        }

        .page-template .footer {
            position: absolute;
            bottom: 30px;
            left: 30px;
            right: 30px;
            border-top: 1px solid #888;
            text-align: center;
            color: #888;
        }

        .page-template .watermark {
            font-weight: bold;
            font-size: 400%;
            text-align: center;
            margin-top: 30%;
            color: #aaaaaa;
            opacity: 0.1;
            transform: rotate(-35deg) scale(1.7, 1.5);
        }
    </style>


@endsection
