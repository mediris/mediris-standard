@extends('layouts.app')

@section('title', ucfirst(trans('titles.consumables')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.consumables')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('consumables.add'),
									'fancybox' => 'add-row cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.consumables')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('string');
				$DescriptionField = new \Kendo\Data\DataSourceSchemaModelField('description');
				$DescriptionField->type('string');
				$UnitsField = new \Kendo\Data\DataSourceSchemaModelField('units');
				$UnitsField->type('string');

				$kendo->addFields(array(
					$IdField,
					$DescriptionField,
					$UnitsField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $consumables = $consumables->toArray();
				
				$canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'consumables', 'show');
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'consumables', 'edit');
				$canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'consumables', 'active');
				
				foreach ($consumables as $key => $value) {
					$title = $consumables[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $consumables[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$editAction = $canEdit ? '<a href=' . route('consumables.edit', [$consumables[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement" ><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
					$activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('consumables.active', [$consumables[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';
					
					$consumables[$key]['actions'] = $editAction . $activeAction;
				}

				// Set  Consumables
				$kendo->setDataSource($consumables);

				// Create Grid
                $kendo->createGrid('consumables');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Description = new \Kendo\UI\GridColumn();
				$Description->field('description')
						->title(ucfirst(trans('labels.description')));
				$Units = new \Kendo\UI\GridColumn();
				if(sizeof($consumables) > 0)
				{
					$Units->field('unit.description')
						->title(ucfirst(trans('labels.units')));
				}
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$Description,
					$Units,
					$Actions,
                ));
				
                $kendo->generate();

				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>
@endsection