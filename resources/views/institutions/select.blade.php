@extends('layouts.app')

@section('title', ucfirst(trans('titles.select')).' '.ucfirst(trans('titles.institution')))

@section('content')

    <div class="container-fluid" id="home">

        <div class="row">
            <div class="col-md-12">
                <h1 class="main-title">{{ ucfirst(trans('titles.select')) }} {{ ucfirst(trans('titles.institution')) }}</h1>
            </div>
        </div>

        <div class="row m-t-20">

            @foreach($institutions as $institution)

                <a href="{{ route('institutions.selected', [$institution->id]) }}" class="institution-link">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="panel panel-stat sombra bd-0">
                            <div class="panel-heading no-bd bg-blue-m">
                                <h3 class="panel-title">{{ $institution->name }}</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="legend legend-address"><p>{{ $institution->address }}</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

            @endforeach

        </div>

    </div>
@endsection