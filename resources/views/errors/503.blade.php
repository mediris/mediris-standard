<!DOCTYPE html>
<html>
    <head>
        <title>En mantenimiento</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                width: 100%;
                height: 100%;
                max-width: 1920px;
                margin: 0 auto;
                background: url(/images/http_errors/503.jpg) center center no-repeat;
                background-size: cover;
            }
        </style>
    </head>
    <body>
        <div class="container">
        </div>
    </body>
</html>
