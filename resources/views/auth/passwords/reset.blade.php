@extends('layouts.app')

@section('title', ucfirst(trans('titles.reset'))." ".ucfirst(trans('titles.password')))

@section('content')
    <div class="container" id="login-block">
        <div class="row">
            <div class="logo">
                <a href="{{ url('/login') }}"><img src="/images/logo_mediris.png" alt="Login"></a>
            </div>
            <div class="col-sm-6 col-md-6 col-sm-offset-3">
                <div class="login-box clearfix">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- BEGIN ERROR BOX -->
                    @if (count($errors) > 0)
                        <!--<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>-->
                    @endif
                    <!-- END ERROR BOX -->
                    <div class="login-logo">
                        <img src="/images/mediris_icon.png" alt="MEDIRIS Logo">
                    </div>
                    <div class="login-form">
                        <form method="post" action="{{ url('/password/reset') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group">
                                <label for='email' class="m-t-20">{{ ucfirst(trans('labels.email')) }}</label>
                                <input type="email" name="email" id="email" class="input-field form-control user m-b-20 btn-style {{ $errors->has('email') ? 'alert alert-danger' : '' }}" value="{{ $email or old('email') }}" />
                                @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password">{{ ucfirst(trans('labels.password')) }}</label>
                                <input type="password" id="password" name="password" class="input-field form-control password {{ $errors->has('password') ? 'alert alert-danger' : '' }}" />
                                @if($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">{{ ucfirst(trans('labels.password-confirmation')) }}</label>
                                <input type="password" id="password_confirmation" name="password_confirmation" class="input-field form-control password {{ $errors->has('password_confirmation') ? 'alert alert-danger' : '' }}" />
                                @if($errors->has('password_confirmation'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                                <div class="btn-caja">
                                    <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left" href="home.html">
                                        <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
