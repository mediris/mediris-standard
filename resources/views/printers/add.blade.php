
<div class="container-fluid">
    <form method="post" action="{{ route('printers.add') }}" id="PrinterAddForm">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.add')).' '.ucfirst(trans('titles.printer')) }}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="kendo-label">
                    <span class="k-checkbox-wrapper">
                        <input type="checkbox" name="active" class="k-checkbox" id="active-chk" value="1">
                        <label for="active-chk" class="k-checkbox-label"></label>
                    </span>
                    <span class="k-in label">{{ ucfirst(trans('labels.is-active')) }}</span>
                </div>
                @if ($errors->has('active'))
                <span class="text-danger">
                    <strong>{{ $errors->first('active') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='description'>{{ ucfirst(trans('labels.name')) }}</label>
                </div>
                <div>
                    <input type="text" name="name" id="name" class="input-field form-control user btn-style" value="{{ old('name') }}"/>
                    @if ($errors->has('name'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='units'>{{ ucfirst(trans('labels.host')) }}</label>
                </div>
                <div>
                    <input type="text" name="host" id="host" class="input-field form-control user btn-style" value="{{ old('host') }}"/>
                    @if ($errors->has('host'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('host') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="modal-footer">
                <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                    <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                </button>
            </div>
        </div>
    </form>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\PrinterAddRequest', '#PrinterAddForm'); !!}

</div>
<script>
    //ColorBoxSelectsInit();
    ColorBoxmultiSelectsInit();
</script>

