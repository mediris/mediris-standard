@extends('layouts.app')

@section('title', ucfirst(trans('titles.patient-states')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.patient-states')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('patientStates.add'),
									'fancybox' => 'add-row cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.patient-states')) }}</h1>
				</div>
				
				<?php
				
				$model = new \Kendo\Data\DataSourceSchemaModel();
				
				$IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('integer');
				
				$DescriptionField = new \Kendo\Data\DataSourceSchemaModelField('description');
				$DescriptionField->type('string');
				
				$model
						->addField($IdField)
						->addField($DescriptionField);
				
				$schema = new \Kendo\Data\DataSourceSchema();
				$schema->model($model);
				
				$dataSource = new \Kendo\Data\DataSource();
				
				$patientStates = $patientStates->toArray();
				
				$canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'patientstates', 'show');
				$canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'patientstates', 'edit');
				$canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'patientstates', 'active');
				
				foreach ($patientStates as $key => $value) {
					$title = $patientStates[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $patientStates[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					$editAction = $canEdit ? '<a href=' . route('patientStates.edit', [$patientStates[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement" ><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
					$activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('patientStates.active', [$patientStates[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';
					
					$patientStates[$key]['actions'] = $editAction . $activeAction;
				}
				
				$dataSource->data($patientStates)
						->pageSize(20)
						->schema($schema);
				
				$grid = new \Kendo\UI\Grid('grid');
				$grid->attr('view-attr', 'patientStates');
				
				$Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				
				$Description = new \Kendo\UI\GridColumn();
				$Description->field('description')
						->title(ucfirst(trans('labels.description')));
				
				
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));
				
				$pageable = new \Kendo\UI\GridPageable();
				$pageable->input(true)
						->numeric(false);
				
				$grid->addColumn($Description)
						->addColumn($Actions)
						->dataSource($dataSource)
						->sortable(true)
						->height(750)
						->filterable(true)
						->pageable($pageable)
						->dataBound('onDataBound');
				?>
				
				{!! $grid->render() !!}
			
			</div>
		</div>
	</div>
@endsection