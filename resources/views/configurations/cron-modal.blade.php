<div class="modal fade" id="config-modal" tabindex="-1" role="dialog" aria-labelledby="config-modal-Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="config-modal-Label">{{ ucwords(trans('cronjob.cronjob-assistant')) }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="minutes">{{  ucfirst(trans('cronjob.minutes')) }}</label>
                        <select name="minutes" id="minutes" class="form-control">
                            <option value="" selected>{{ ucfirst(trans('labels.select')) }}</option>
                            <option value="*/1 * * * *">{{ ucfirst(trans('cronjob.cron-1-min')) }}</option>
                            <option value="*/2 * * * *">{{ ucfirst(trans('cronjob.cron-2-min')) }}</option>
                            <option value="*/3 * * * *">{{ ucfirst(trans('cronjob.cron-3-min')) }}</option>
                            <option value="*/4 * * * *">{{ ucfirst(trans('cronjob.cron-4-min')) }}</option>
                            <option value="*/5 * * * *">{{ ucfirst(trans('cronjob.cron-5-min')) }}</option>
                            <option value="*/6 * * * *">{{ ucfirst(trans('cronjob.cron-6-min')) }}</option>
                            <option value="*/10 * * * *">{{ ucfirst(trans('cronjob.cron-10-min')) }}</option>
                            <option value="*/15 * * * *">{{ ucfirst(trans('cronjob.cron-15-min')) }}</option>
                            <option value="*/20 * * * *">{{ ucfirst(trans('cronjob.cron-20-min')) }}</option>
                            <option value="*/30 * * * *">{{ ucfirst(trans('cronjob.cron-30-min')) }}</option>
                            <option value="*/45 * * * *">{{ ucfirst(trans('cronjob.cron-45-min')) }}</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="hours">{{ ucfirst(trans('cronjob.hours')) }}</label>
                        <select name="hours" id="hours" class="form-control">
                            <option value="" selected>{{ ucfirst(trans('labels.select')) }}</option>
                            <option value="0 */1 * * *">{{ ucfirst(trans('cronjob.cron-1-hour')) }}</option>
                            <option value="0 */2 * * *">{{ ucfirst(trans('cronjob.cron-2-hour')) }}</option>
                            <option value="0 */4 * * *">{{ ucfirst(trans('cronjob.cron-4-hour')) }}</option>
                            <option value="0 */6 * * *">{{ ucfirst(trans('cronjob.cron-6-hour')) }}</option>
                            <option value="0 */8 * * *">{{ ucfirst(trans('cronjob.cron-8-hour')) }}</option>
                            <option value="*/12 * * * *">{{ ucfirst(trans('cronjob.cron-12-hour')) }}</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="days">{{  ucfirst(trans('cronjob.days')) }}</label>
                        <select name="days" id="days" class="form-control">
                            <option value="" selected>{{ ucfirst(trans('labels.select')) }}</option>
                            <option value="0 0 */1 * *">{{ ucfirst(trans('cronjob.cron-1-day-midnight')) }}</option>
                            <option value="0 12 */1 * *">{{ ucfirst(trans('cronjob.cron-1-day-noon')) }}</option>
                            <option value="0 0 */2 * *">{{ ucfirst(trans('cronjob.cron-2-day')) }}</option>
                            <option value="0 0 */3 * *">{{ ucfirst(trans('cronjob.cron-3-day')) }}</option>
                            <option value="0 0 *5 * *">{{ ucfirst(trans('cronjob.cron-5-day')) }}</option>
                            <option value="0 0 */10 * *">{{ ucfirst(trans('cronjob.cron-10-day')) }}</option>
                            <option value="0 0 */15 * *">{{ ucfirst(trans('cronjob.cron-15-day')) }}</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="months">{{ ucfirst(trans('cronjob.months')) }}</label>
                        <select name="months" id="months" class="form-control">
                            <option value="" selected>{{ ucfirst(trans('labels.select')) }}</option>
                            <option value="0 12 15 * *">{{ ucfirst(trans('cronjob.cron-15-of-month-noon')) }}</option>
                            <option value="0 12 30 * *">{{ ucfirst(trans('cronjob.cron-30-of-month-noon')) }}</option>
                            <option value="0 0 15 * *">{{ ucfirst(trans('cronjob.cron-15-of-month-midnight')) }}</option>
                            <option value="0 0 30 * *">{{ ucfirst(trans('cronjob.cron-30-of-month-midnight')) }}</option>
                            <option value="0 12 1 * *">{{ ucfirst(trans('cronjob.cron-1-of-month-noon')) }}</option>
                            <option value="0 0 1 * *">{{ ucfirst(trans('cronjob.cron-1-of-month-midnight')) }}</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="dow">{{ trans('cronjob.dow') }}</label>
                        <select name="dow" id="dow" class="form-control">
                            <option value="" selected>{{ ucfirst(trans('labels.select')) }}</option>
                            <option value="0 0 * * 1">{{ ucfirst(trans('cronjob.monday')) }}</option>
                            <option value="0 0 * * 2">{{ ucfirst(trans('cronjob.tuesday')) }}</option>
                            <option value="0 0 * * 3">{{ ucfirst(trans('cronjob.wednesday')) }}</option>
                            <option value="0 0 * * 4">{{ ucfirst(trans('cronjob.thursday')) }}</option>
                            <option value="0 0 * * 5">{{ ucfirst(trans('cronjob.friday')) }}</option>
                            <option value="0 0 * * 6">{{ ucfirst(trans('cronjob.saturday')) }}</option>
                            <option value="0 0 * * 0">{{ ucfirst(trans('cronjob.sunday')) }}</option>
                            <option value="0 0 * * 1-5">{{ ucfirst(trans('cronjob.mon-fri-midnight')) }}</option>
                            <option value="0 12 * * 1-5">{{ ucfirst(trans('cronjob.mon-fri-noon')) }}</option>
                            <option value="0 12 * * 6,0">{{ ucfirst(trans('cronjob.sat-sun-noon')) }}</option>
                            <option value="0 0 * * 6,0">{{ ucfirst(trans('cronjob.sat-sun-midnight')) }}</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="miscellaneous">{{ trans('cronjob.miscellaneous') }}</label>
                        <select name="miscellaneous" id="miscellaneous" class="form-control">
                            <option value="" selected>{{ ucfirst(trans('labels.select')) }}</option>
                            <option value="0 0 * * 1">{{ ucfirst(trans('cronjob.monday')) }}</option>
                            <option value="0 0 * * 2">{{ ucfirst(trans('cronjob.tuesday')) }}</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                	    <label for="command">{{ trans('cronjob.command') }}</label>
                        <input type="text" name="command" id="command" class="form-control" value="">
                	</div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="cron-finished">{{ trans('cronjob.cronjob') }}</label>
                        <input type="text" name="cron-finished" id="cron-finished" class="form-control" value="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close-cron"
                        data-dismiss="modal">{{ ucfirst(trans('labels.close')) }}</button>
                <button type="button" class="btn btn-form" id="set-cronjob">{{ ucfirst(trans('labels.save')) }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    <input type="hidden" id="cron-type" value="" readonly>
</div><!-- /.modal -->
