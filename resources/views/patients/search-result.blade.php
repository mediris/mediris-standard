@if( $patients && $patients->count() > 0 )

    <?php

    $model = new \Kendo\Data\DataSourceSchemaModel();

    $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
    $IdField->type('integer');

    $PatientIdField = new \Kendo\Data\DataSourceSchemaModelField('patient_ID');
    $PatientIdField->type('string');

    $NameField = new \Kendo\Data\DataSourceSchemaModelField('name');
    $NameField->type('string');

    $EmailField = new \Kendo\Data\DataSourceSchemaModelField('email');
    $EmailField->type('string');

    $AgeField = new \Kendo\Data\DataSourceSchemaModelField('age');
    $AgeField->type('string');

    $model
            ->addField($IdField)
            ->addField($PatientIdField)
            ->addField($NameField)
            ->addField($AgeField)
            ->addField($EmailField);

    $schema = new \Kendo\Data\DataSourceSchema();
    $schema->model($model);

    $dataSource = new \Kendo\Data\DataSource();
    
    foreach ($patients as $key => $value) {
        if ( $patients[$key]->active ) {
            $href = "/reception/add/" . $patients[$key]->id . "/" . $appointment_id;
            $patients[$key]->actions = 
                "<a href='" . $href . "' class='btn btn-form' id='choose-patient'>".
                    "<i class='fa fa-check' aria-hidden='true'></i>" . ucfirst(trans('labels.select')) .
                "</a>";
        } else {
            $patients[$key]->actions = ucfirst(trans('labels.disabled'));
        }

        $photo = $patients[$key]->photo == '' ? '/images/patients/default_avatar.jpeg' : asset( 'storage/'. $patients[$key]->photo );
        $patients[$key]->name =
            '<div class="customer-photo" style="background-image: url(' . $photo . ');"></div>'.
            '<div class="customer-name">' . $patients[$key]->firstName . ' ' . $patients[$key]->lastName . '</div>';
        $patients[$key]->name = html_entity_decode($patients[$key]->name);
    }

    $dataSource->data($patients)
            ->pageSize(7)
            ->schema($schema)
            ->serverFiltering(true)
            ->serverGrouping(true)
            ->serverSorting(true)
            ->serverPaging(true);

    $grid = new \Kendo\UI\Grid('grid');

    $Id = new \Kendo\UI\GridColumn();
    $Id->field('id')
            ->filterable(false)
            ->title('Id');

    $PatientId = new \Kendo\UI\GridColumn();
    $PatientId->field('patientID')
            ->title(ucfirst(trans('labels.patient-id')));

    $Name = new \Kendo\UI\GridColumn();
    $Name->field('name')
            ->encoded(false)
            ->title(ucfirst(trans('labels.name')));

    $Email = new \Kendo\UI\GridColumn();
    $Email->field('email')
            ->encoded(false)
            ->title(ucfirst(trans('labels.email')));

    $Age = new \Kendo\UI\GridColumn();
    $Age->field('age')
            ->encoded(false)
            ->title(ucfirst(trans('labels.age')));

    $Actions = new \Kendo\UI\GridColumn();
    $Actions->field('actions')
            ->sortable(false)
            ->filterable(false)
            ->encoded(false)
            ->title(ucfirst(trans('labels.actions')));

    $pageable = new \Kendo\UI\GridPageable();
    $pageable->input(true)
            ->numeric(false);

    $grid->addColumn($PatientId)
            ->addColumn($Name)
            ->addColumn($Email)
            ->addColumn($Age)
            ->addColumn($Actions)
            ->dataSource($dataSource)
            ->sortable(true)
            ->scrollable(true)
            ->height(500)
            ->filterable(true)
            ->pageable($pageable);

    ?>

    {!! $grid->render() !!}

@else

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="alert alert-warning">
                <strong>Atenci&oacute;n!</strong> No hay resultados para el criterio de b&uacute;squeda seleccionado.
            </div>
        </div>
    </div>

@endif