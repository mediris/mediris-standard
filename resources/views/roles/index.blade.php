@extends('layouts.app')

@section('title', ucfirst(trans('titles.roles')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.roles')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('roles.add'),
									'fancybox' => '',
									'routeBack' => ''
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.roles')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('integer');		
				$NameField = new \Kendo\Data\DataSourceSchemaModelField('name');
				$NameField->type('string');

				$kendo->addFields(array(
					$IdField,
					$NameField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $roles = $roles->toArray();
				
				foreach ($roles as $key => $value) {
					$title = $roles[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $roles[$key]['active'] == 1 ? "" : "active";
					$roles[$key]['actions'] = '<a href=' . route('roles.edit', [$roles[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . '><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>';
				}

				// Set Institutions
				$kendo->setDataSource($roles);

				// Create Grid
                $kendo->createGrid('roles');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$Name = new \Kendo\UI\GridColumn();
				$Name->field('name')
						->title(ucfirst(trans('labels.name')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));
				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
	                //$Id,
					$Name,
					$Actions,
                ));
				
                $kendo->generate();

				?>
				
				{!! $kendo->render() !!}			
			
			</div>
		</div>
	</div>

@endsection