<?php

return [
	'paths' => [
		'order' 					=> '/orders/final',
		'addendum' 					=> '/orders/addendums',
		'patient_avatar'			=> '/patients/avatars',
		'patient_document'			=> '/patients/documents',
		'servicerequest_document' 	=> '/servicerequest/documents',
		'requestedprocedure_audio' 	=> '/requestedprocedure/audio',
		'institution_logo'			=> '/institutions/logos',
		'user_signature'			=> '/users/signatures',
	]
];
?>