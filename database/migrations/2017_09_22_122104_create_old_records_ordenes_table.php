<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldRecordsordenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_records_ordenes', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->index();
            $table->string('accession_number',225)->nullable();
            $table->string('study_instance_id',225)->nullable();
            $table->dateTime('fecharealizacion')->nullable();
            $table->bigInteger('id_procedimiento')->unsigned()->nullable();
            $table->longText('texto')->nullable();
            $table->bigInteger('id_usr')->unsigned()->nullable();
            $table->string('tipo',20)->nullable();
            //$table->bigInteger('id_addemdum');
            $table->bigInteger('id_estatus')->unsigned()->nullable();
            $table->bigInteger('id_solicitud')->unsigned()->nullable();
            //$table->bigInteger('id_informe_transcrito');
            $table->longText('observaciones')->nullable();
            $table->string('campolibrearchivodocente',225)->nullable();
            $table->integer('birad')->nullable();
            $table->dateTime('fecha')->nullable();
            $table->bigInteger('id_institucion')->unsigned()->index();
            $table->timestamps();
            $table->primary(['id','id_institucion']);
            $table->boolean('bi_rad_notificado')->default(false)->comment = "Indicates that a reminder notification was sent to redo the test";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_records_ordenes');
    }
}
