<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmUrlInvox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('institutions', function (Blueprint $table) {
            $table->string('url_invox', 250);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('institutions', function (Blueprint $table) {
            $table->dropColumn('url_invox');
        });
    }
}
