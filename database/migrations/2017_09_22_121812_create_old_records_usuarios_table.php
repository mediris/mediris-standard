<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldRecordsusuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_records_usuarios', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->index();
            $table->string('nombre',225)->nullable();
            $table->string('apellido',225)->nullable();
            $table->string('email',225)->nullable();
            $table->string('cedula',20)->nullable();
            $table->string('telefono',20)->nullable();
            $table->string('celular',20)->nullable();
            $table->string('idadministrativo',225)->nullable();
            $table->string('firma', 250);
            $table->bigInteger('id_institucion')->unsigned()->index();
            $table->timestamps();
            $table->primary(['id','id_institucion']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_records_usuarios');
    }
}
