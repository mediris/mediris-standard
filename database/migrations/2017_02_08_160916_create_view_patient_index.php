<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewPatientIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        DB::statement("
            CREATE VIEW `patientsIndexView` AS
            SELECT 
                `p`.`id` AS `id`,
                `p`.`active` AS `active`,
                `p`.`patient_ID` AS `patientID`,
                `p`.`last_name` AS `lastName`,
                `p`.`first_name` AS `firstName`,
                `p`.`email` AS `email`,
                `p`.`telephone_number` AS `phoneNumber`,
                `p`.`cellphone_number` AS `cellPhoneNumber`,
                `p`.`birth_date` AS `birthDate`,
                `p`.`photo` AS `photo`,
                TIMESTAMPDIFF(YEAR, `p`.`birth_date`, NOW()) AS `age`
            FROM
                `patients` `p`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS patientsIndexView");
    }
}
