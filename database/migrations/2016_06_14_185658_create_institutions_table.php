<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');
            $table->string('institution_id', 45);
            $table->string('telephone_number', 45);
            $table->string('telephone_number_2', 45);
            $table->string('email', 45)->unique()->index();
            $table->string('url', 250);
            $table->string('name', 50)->unique()->index();
            $table->string('address', 250);
            $table->string('logo', 250);
            //$table->boolean('logo_actives_report')->default(true);
            $table->string('logo_email', 250);
            $table->string('report_header', 250);
            $table->string('report_footer', 250);
            $table->string('administrative_id', 45)->unique()->index();
            $table->integer('group_id')->unsigned()->index();
            $table->string('url_pacs', 250);
            $table->string('url_external_api', 250)->nullable();
            $table->string('base_structure', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
    }
}
