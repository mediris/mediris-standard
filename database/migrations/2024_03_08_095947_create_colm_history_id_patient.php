<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmHistoryIdPatient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        $this->down();
        
        Schema::table('patients', function (Blueprint $table) {

            $table->string('history_id')->unique()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::table('patients', function (Blueprint $table) {
        
            if (Schema::hasColumn('patients', 'history_id')) {
                
                $table->dropColumn('history_id');
            }
        
        });
    }
}
