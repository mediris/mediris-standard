<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmRequPreadmisionTrasncribe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::table('institutions', function (Blueprint $table) {

        $table->integer('require_preadmission');
        $table->integer('require_transcriber');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::table('institutions', function (Blueprint $table) {

            if (Schema::hasColumn('institutions', 'require_preadmission', 'require_transcriber' )) {
                
            $table->dropColumn('require_preadmission');
            $table->dropColumn('require_transcriber');

            }

        });
    }
}
