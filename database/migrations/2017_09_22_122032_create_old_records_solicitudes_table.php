<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldRecordssolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_records_solicitudes', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->index();
            $table->dateTime('fecha_solicitud')->nullable();
            $table->string('idadministrativo',225)->nullable();
            $table->bigInteger('id_paciente')->unsigned()->nullable();
            $table->bigInteger('id_usuario')->unsigned()->nullable();
            $table->dateTime('fechaaprobacionadministrativa')->nullable();
            $table->string('referenteexterno',225)->nullable();
            $table->bigInteger('id_institucion')->unsigned()->index();
            $table->timestamps();
            $table->primary(['id','id_institucion']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_records_solicitudes');
    }
}
