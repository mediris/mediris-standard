<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolResidenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
                    'id'=> 98,
                    'name' => 'radiologo residente',
                    'active' => 1
                ]);
    }
}
