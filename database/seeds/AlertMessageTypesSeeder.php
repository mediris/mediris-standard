<?php

use Illuminate\Database\Seeder;
use App\AlertMessageType;

class AlertMessageTypesSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        AlertMessageType::create([
            'name' => 'General',
            'icon' => '&#xf075',
            'css_class' => 'normal-alert-message'
        ]);

        AlertMessageType::create([
            'name' => 'Importante',
            'icon' => '&#xf06a',
            'css_class' => 'important-alert-message',
            'priority' => 2
        ]);

        AlertMessageType::create([
            'name' => 'Alerta',
            'icon' => '&#xf06a',
            'css_class' => 'full-alert-message',
            'priority' => 2
        ]);
    }
}
