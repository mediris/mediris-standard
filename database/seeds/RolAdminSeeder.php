<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
                    'id'=> 99,
                    'name' => 'administrador',
                    'active' => 1
                ]);
    }
}
