<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Section;
use App\Action;

class RoleActionSectionResidenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate Roles
        $roles     = array();
        $tmp_roles = Role::all();
        foreach ($tmp_roles as $key => $value) {
            $roles[$value->name] = $value->id;
        }

        // Generate Actions
        $actions     = array();
        $tmp_actions = Action::all();
        foreach ($tmp_actions as $key => $value) {
            $actions[$value->name] = $value->id;
        }

        // Generate Sections
        $sections     = array();
        $tmp_sections = Section::all();
        foreach ($tmp_sections as $key => $value) {
            $sections[$value->name] = $value->id;
        }

            //Se asignan las acciones para el Rol
       /*
        *  1 system                
        *  2 institutions          
        *  3 modalities            
        *  4 users                 
        *  5 roles                 
        *  6 configurations        
        *  7 divisions             
        *  8 printers              
        *  9 rooms                 
        * 10 consumables           
        * 11 units                 
        * 12 patientTypes          
        * 13 migrations            
        * 14 administration        
        * 15 sources               
        * 16 templates             
        * 17 notification_templates
        * 18 suspend_reasons       
        * 19 equipment             
        * 20 alertMessages         
        * 21 steps                 
        * 22 procedures            
        * 23 orders                
        * 24 referrings            
        * 25 patientStates         
        * 26 docent-file           
        * 27 categories            
        * 28 subcategories         
        * 29 patients              
        * 30 legacypatients        
        * 31 reception             
        * 32 appointments          
        * 33 technician            
        * 34 radiologist           
        * 35 finalreport           
        * 36 preadmission          
        * 37 search                
        * 38 reports               
        * 39 transcriber           
        * 40 results     
        * 41 polls
        */

        $role_section = array();

        // 98 - radiologo residente
        /* tecnico 
         * radiologo 
         * resultados 
         * paciente
         * busqueda
         */
        $role_section[$roles['radiologo residente']] = [
            $sections["technician"],
            $sections["radiologist"],
            $sections["results"],
            $sections["patients"],
            $sections["templates"],
            $sections["docent-file"],
            $sections["finalreport"],
            $sections["reports"],
            $sections["search"]
        ];

        foreach ($role_section as $role => $sections)
        {
            foreach ($sections as $section) {
                $actions_sections = Section::find($section)->actions;
                foreach ( $actions_sections as $action_section)
                {
                    Role::find($role)->roleForSection()->attach(['action_section_id' => $action_section->pivot->id]);
                }
            }
        }
    }
}
