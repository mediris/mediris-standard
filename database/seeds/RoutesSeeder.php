<?php

use Illuminate\Database\Seeder;
use App\Patient;
use App\Appointment;
use Carbon\Carbon;

class RoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $routeCollection  = Route::getRoutes();
        $middlewareName   = [
            'permission', 
            'root',
            'admin'
        ];
        $actions_sections = [];
        $actions          = [];
        $sections         = [];

        foreach ($routeCollection as $route)
        {
            $middleware = $route->middleware();
            // Has a Middleware at least
            if (count($middleware) > 0)
            {
              // Check if MiddleWare coincidence exist
              $pass = false;
              foreach ($middlewareName as $compare) {
                  if(in_array($compare, $middleware)){
                    $pass = true;
                  }
              }
              if ($pass)
              {
                // Action
                $data         = $route->getAction();
                $action_value = explode('@', $route->getActionName())[1];
                if(isset($data['menu'])){
                    $display = $data['menu'];
                } else if ($action_value == 'index'){
                    $display = true;
                } else {
                    $display = false;
                }
                $action   = array(
                    'value'     => $action_value,
                    'display'   => $display
                );
                if(!in_array($action['value'], array_column($actions, 'value'))){ $actions[] = $action; }
                $action_id  = array_search($action, $actions) + 1;
                // Group By
                $insert = true;
                $sub    = false;
                if(isset($data['priority'])){
                    $priority = $data['priority'];
                } else {
                    $priority = 0;
                }
                if(isset($data['groupBy'])){
                    if(isset($data['submenu']) && isset($data['parent'])){
                        $submenu = $data['submenu'];
                        $parent  = array_search($data['groupBy'], array_column($sections, 'value'));
                    } else{ 
                        $parent = array_search($data['groupBy'], array_column($sections, 'value'));
                    }
                    if($parent === false && !isset($data['submenu']) && !isset($data['parent'])){
                        $section = array(
                            'value'     => $data['groupBy'], 
                            'parent'    => 0,
                            'active'    => 0,
                            'priority'  => $priority,
                        );
                        $sections[] = $section;
                        $parent = count($sections);
                        // Action Section
                        $action_section = (object) array(
                          'action'  => (object) array('id' => 1, 'value' => $action),
                          'section' => (object) array('id' => count($sections), 'value' => $section),
                        );
                        if(!in_array($action_section, $actions_sections)){ $actions_sections[] = $action_section; $insert = false; }
                    } else if(isset($data['submenu']) && isset($data['parent'])){
                        $section = array(
                            'value'     => $data['parent'], 
                            'parent'    => $parent + 1,
                            'active'    => 0,
                            'priority'  => $priority,
                        );
                        if(!in_array($section['value'], array_column($sections, 'value'))){ $sections[] = $section; }
                        $parent = array_search($data['parent'], array_column($sections, 'value'));
                        // Action Section
                        $action_section = (object) array(
                          'action'  => (object) array('id' => 1, 'value' => $action),
                          'section' => (object) array('id' => count($sections), 'value' => $section),
                        );
                        if(!in_array($action_section, $actions_sections)){ $actions_sections[] = $action_section; $insert = false; $sub = true; }
                    } else {
                        $parent++;
                    }
                } else {
                    $parent = 0;
                }
                if($sub) {
                    $parent++;
                }
                // Data
                $section  = array(
                    'value'     => explode('/', $route->getPrefix())[1],
                    'parent'    => $parent,
                    'active'    => 1,
                    'priority'  => $priority,
                );
                // Control Data
                if(!in_array($section['value'], array_column($sections, 'value'))){ $sections[] = $section; }
                // Index
                $section_id = array_search($section['value'], array_column($sections, 'value'));
                if($section_id === false){
                    $section_id = 0;
                } else {
                    $section_id++;
                }
                // Routes
                if($insert){
                    $action_section = (object) array(
                      'action'  => (object) array('id' => $action_id, 'value' => $action),
                      'section' => (object) array('id' => $section_id, 'value' => $section),
                    );
                    if(!in_array($action_section, $actions_sections)){ $actions_sections[] = $action_section; }
                }
              }
            }
        }

        // Call Seeders
        (new SectionsSeeder())->run($sections);
        (new ActionsSeeder())->run($actions);
        (new ActionSectionSeeder())->run($actions_sections);

    }

}