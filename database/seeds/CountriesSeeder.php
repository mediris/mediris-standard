<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        // se carga el seed a partir del archivo countries.csv
        Excel::load('database/exportFile/countries.csv', function ( $reader ){

            foreach( $reader->get() as $seed ) {
                Country::create([
                    'id' => $seed->id,
                    'capital' => $seed->capital,
                    'capital_es' => $seed->capital_es,
                    'citizenship' => $seed->citizenship,
                    'citizenship_es' => $seed->citizenship_es,
                    'country_code' => $seed->country_code,
                    'currency' => $seed->currency,
                    'currency_code' => $seed->currency_code,
                    'currency_sub_unit' => $seed->currency_sub_unit,
                    'full_name' => $seed->full_name,
                    'iso_3166_2' => $seed->iso_3166_2,
                    'iso_3166_3' => $seed->iso_3166_3,
                    'name' => $seed->name,
                    'name_es' => $seed->name_es,
                    'region_code' => $seed->region_code,
                    'sub_region_code' => $seed->sub_region_code,
                    'eea' => $seed->eea,
                    'calling_code' => $seed->calling_code,
                    'currency_symbol' => $seed->currency_symbol,
                    'flag' => $seed->flag,
                ]);
            }
        });
    }
}
