<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Institution;
use App\Section;
use App\Action;

class RoleInstitutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $sections = Section::all();
        $actions = Action::all();

        $total = count($sections) * count($actions);

        for($i = 0; $i<$total; $i++){
            Role::find(1)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(2)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(3)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(4)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(5)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(6)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(7)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(8)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(9)->institutions()->attach(1,['action_section_id' => $i+1]);
            Role::find(10)->institutions()->attach(1,['action_section_id' => $i+1]);

            Role::find(1)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(2)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(3)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(4)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(5)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(6)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(7)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(8)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(9)->institutions()->attach(2,['action_section_id' => $i+1]);
            Role::find(10)->institutions()->attach(2,['action_section_id' => $i+1]);

            Role::find(1)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(2)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(3)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(4)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(5)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(6)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(7)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(8)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(9)->institutions()->attach(3,['action_section_id' => $i+1]);
            Role::find(10)->institutions()->attach(3,['action_section_id' => $i+1]);
        }
    }
}
