console.info('main.js loaded!');

function badWay() {

    $('#Load').click(function() {
        $('.section-box1').load('content1.html');
        $('.section-box2').load('content2.html');
        $('.section-box3').load('content3.html', function() {
            $('#Proceed').removeAttr('disabled');
        });
    });
}

function deferredMethod() {
    $('#Load').click(function() {
        $.when(
                $.get('content1.html', function(result) {
                    $('.section-box1').html(result);
                }, 'html'),
                $.get('content2.html', function(result) {
                    $('.section-box2').html(result);
                }, 'html'),
                $.get('content3.html', function(result) {
                    $('.section-box3').html(result);
                }, 'html')
            )
            .then(function() {
                $('#Proceed').removeAttr('disabled');
            });
    });
}

function improvedDeferredMethod() {

    var loadSection = function(options) {
        if (typeof options !== 'object') { options = {}; }
        options.selector = options.selector || '';
        options.url = options.url || '';
        return $.get(options.url, function(result) {
            $(options.selector).html(result)
        }, 'html');
    }

    $('#Load').click(function() {
        $.when(
                loadSection({ selector: '.section-box1', url: 'content1.html' }),
                loadSection({ selector: '.section-box2', url: 'content2.html' }),
                loadSection({ selector: '.section-box3', url: 'content3.html' })
            )
            .then(function() {
                $('#Proceed').removeAttr('disabled');
            });
    });
}

function improvedDeferredMethodWithErrorsHandler() {

    var loadSection = function(options) {
        if (typeof options !== 'object') { options = {}; }
        options.selector = options.selector || '';
        options.url = options.url || '';
        return $.get(options.url, function(result) {
            $(options.selector).html(result)
        }, 'html');
    }

    $('#Load').click(function() {
        $.when(
                loadSection({ selector: '.section-box1', url: 'content1.html' }),
                loadSection({ selector: '.section-box2', url: 'content2.html' }),
                loadSection({ selector: '.section-box3', url: 'content3.html' })
            )
            .then(function() {
                $('#Proceed').removeAttr('disabled');
            })
            .fail(function(result) {
                $('#Messages')
                    .append('Failiur! <br />')
                    .append('Result:' + result.statusText + '<br />');

            });
    });
}

function DeferredMethodFinal() {

    var loadSection = function(options) {
        if (typeof options !== 'object') { options = {}; }
        options.selector = options.selector || '';
        options.url = options.url || '';
        return $.get(options.url, function(result) {
            $(options.selector).html(result)
        }, 'html');
    }

    $('#Load').click(function() {
        var myDefer = $.when(
                loadSection({ selector: '.section-box1', url: 'content1a.html' }),
                loadSection({ selector: '.section-box2', url: 'content2.html' }),
                loadSection({ selector: '.section-box3', url: 'content3.html' })
            )
            .promise() // Prevents to anything change the state of the deferred object
            .done()
            .done(function() {
                $('#Messages').append('Second done handler fired! <br />');
            })
            .fail(function(result) {
                $('#Messages')
                    .append('Failiur! <br />')
                    .append('Result:' + result.statusText + '<br />');

            })
            .always(function() {
                $('#Proceed').removeAttr('disabled');
            });

        myDefer.done(function() {
            $('#Messages').append('Aditional  done handler called! using myDefer.done()  <br />');
        });
    });
}

// jQuery Ready
$(function() {

    // badWay();
    // deferredMethod();
    // improvedDeferredMethod();
    // improvedDeferredMethodWithErrorsHandler();
    DeferredMethodFinal();


});